# web-shared-util-ngrx

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test web-shared-util-ngrx` to execute the unit tests via [Jest](https://jestjs.io).

import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { DataPersistence, NxModule } from '@nrwl/angular';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreModule } from '@ngrx/store';
import { environment } from '@mateusz-d-todo-list/web/shared/config';

@NgModule({
  imports: [
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([]),
    NxModule.forRoot(),
    StoreModule.forRoot(
      {},
      {
        runtimeChecks: {
          strictActionImmutability: true,
          strictStateImmutability: true,
        },
      },
    ),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
    }),
  ],
  providers: [DataPersistence],
})
export class MainStoreModule {}

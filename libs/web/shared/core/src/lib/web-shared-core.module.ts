import { NgModule, Optional, SkipSelf } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MainStoreModule } from './modules/main-store.module';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MainStoreModule,
  ],
})
export class WebSharedCoreModule {
  constructor(
    @Optional()
    @SkipSelf()
    webSharedCoreModule: WebSharedCoreModule,
  ) {
    if (webSharedCoreModule) {
      throw new Error('WebSharedCoreModule should only be imported once');
    }
  }
}

import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Task, TaskTitlePayload } from '@mateusz-d-todo-list/web/task/domain';

@Component({
  selector: 'mateusz-d-todo-list-task-card',
  templateUrl: './task-card.component.html',
  styleUrls: ['./task-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaskCardComponent {
  @Output() markedAsDone = new EventEmitter<void>();
  @Output() markedAsTodo = new EventEmitter<void>();
  @Output() removed = new EventEmitter<void>();
  @Output() updated = new EventEmitter<TaskTitlePayload>();

  @Input() set task(value: Task | null) {
    this.editMode = false;
    this.taskDetails = value ?? null;
    this.formGroup.patchValue({
      name: value?.name ?? null,
    });
  }

  taskDetails: Task | null;
  formGroup: FormGroup;
  editMode: boolean;

  constructor(private formBuilder: FormBuilder) {
    this.initForm();
  }

  onMarkAsDone(): void {
    if (this.taskDetails && !this.taskDetails.done) {
      this.markedAsDone.emit();
    }
  }

  onMarkAsTodo(): void {
    if (this.taskDetails?.done) {
      this.markedAsTodo.emit();
    }
  }

  onRemove(): void {
    this.removed.emit();
  }

  onToggleEditMode(): void {
    this.editMode = !this.editMode;
  }

  onSubmit(): void {
    if (this.formGroup.valid) {
      const formValue: TaskTitlePayload = { ...this.formGroup.value };
      this.updated.emit(formValue);
      this.editMode = false;
    } else {
      this.formGroup.markAllAsTouched();
    }
  }

  private initForm(): void {
    this.formGroup = this.formBuilder.group({
      name: [null, [Validators.required]],
    });
  }
}

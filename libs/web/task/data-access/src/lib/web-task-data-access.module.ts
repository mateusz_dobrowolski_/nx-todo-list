import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  TASKLIST_FEATURE_KEY,
  initialState as taskListInitialState,
  taskListReducer,
} from './+state/task-list.reducer';
import { TaskListEffects } from './+state/task-list.effects';
import { TaskListFacade } from './+state/task-list.facade';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(TASKLIST_FEATURE_KEY, taskListReducer, {
      initialState: taskListInitialState,
    }),
    EffectsModule.forFeature([TaskListEffects]),
  ],
  providers: [TaskListFacade],
})
export class WebTaskDataAccessModule {}

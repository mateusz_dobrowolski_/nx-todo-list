import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {
  GetTaskPayload,
  RemoveTaskPayload,
  Task,
  TaskTitlePayload,
} from '@mateusz-d-todo-list/web/task/domain';

@Injectable({
  providedIn: 'root',
})
export class TaskListDataService {
  readonly endpoints = {
    getTask: (taskId: number | string) => `api/tasks/${taskId}`,
    getTaskCollection: () => 'api/tasks',
    createTask: () => 'api/tasks',
    updateTask: (taskId: number | string) => `api/tasks/${taskId}`,
    removeTask: (taskId: number | string) => `api/tasks/${taskId}`,
  };

  constructor(private http: HttpClient) {}

  getTask(payload: GetTaskPayload): Observable<Task> {
    return this.http.get<Task>(this.endpoints.getTask(payload.id));
  }

  getTaskCollection(): Observable<Task[]> {
    return this.http.get<Task[]>(this.endpoints.getTaskCollection());
  }

  createTask(payload: TaskTitlePayload): Observable<Task> {
    return this.http.post<Task>(this.endpoints.createTask(), payload);
  }

  updateTask(payload: Task): Observable<Task> {
    return this.http.put<Task>(this.endpoints.updateTask(payload.id), payload);
  }

  removeTask(payload: RemoveTaskPayload): Observable<Task> {
    return this.http.delete<Task>(this.endpoints.removeTask(payload.id));
  }
}

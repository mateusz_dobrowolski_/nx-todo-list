import { TestBed } from '@angular/core/testing';

import { TaskListDataService } from './task-list-data.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

describe('TaskListDataService', () => {
  let httpMock: HttpTestingController;

  let service: TaskListDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [HttpClientTestingModule] });
    httpMock = TestBed.inject(HttpTestingController);
    service = TestBed.inject(TaskListDataService);
  });
  afterEach(() => {
    httpMock.verify();
    localStorage.clear();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getTask', () => {
    test('returns an observable of response data on success', () => {
      const response = {} as any;

      service.getTask({ id: 1 }).subscribe(res => {
        expect(res).toBe(response);
      });

      const req = httpMock.expectOne(service.endpoints.getTask(1));
      expect(req.request.method).toBe('GET');
      req.flush(response);
    });

    test('throws an error including response data on fail', () => {
      const response = {};

      service.getTask({ id: 1 }).subscribe(
        () => {
          fail('expecting error');
        },
        err => {
          expect(err.error).toBe(response);
        },
      );

      const req = httpMock.expectOne(service.endpoints.getTask(1));
      expect(req.request.method).toBe('GET');
      req.flush(response, {
        status: 400,
        statusText: 'Bad request',
      });
    });
  });

  describe('#getTaskCollection', () => {
    test('returns an observable of response data on success', () => {
      const response = {} as any;

      service.getTaskCollection().subscribe(res => {
        expect(res).toBe(response);
      });

      const req = httpMock.expectOne(service.endpoints.getTaskCollection());
      expect(req.request.method).toBe('GET');
      req.flush(response);
    });

    test('throws an error including response data on fail', () => {
      const response = {};

      service.getTaskCollection().subscribe(
        () => {
          fail('expecting error');
        },
        err => {
          expect(err.error).toBe(response);
        },
      );

      const req = httpMock.expectOne(service.endpoints.getTaskCollection());
      expect(req.request.method).toBe('GET');
      req.flush(response, {
        status: 400,
        statusText: 'Bad request',
      });
    });
  });

  describe('#createTask', () => {
    test('returns an observable of response data on success', () => {
      const response = {} as any;

      service.createTask({} as any).subscribe(res => {
        expect(res).toBe(response);
      });

      const req = httpMock.expectOne(service.endpoints.createTask());
      expect(req.request.method).toBe('POST');
      req.flush(response);
    });

    test('throws an error including response data on fail', () => {
      const response = {};

      service.createTask({} as any).subscribe(
        () => {
          fail('expecting error');
        },
        err => {
          expect(err.error).toBe(response);
        },
      );

      const req = httpMock.expectOne(service.endpoints.createTask());
      expect(req.request.method).toBe('POST');
      req.flush(response, {
        status: 400,
        statusText: 'Bad request',
      });
    });
  });

  describe('#updateTask', () => {
    test('returns an observable of response data on success', () => {
      const response = {} as any;

      service.updateTask({ id: 1, name: 'foo', done: true }).subscribe(res => {
        expect(res).toBe(response);
      });

      const req = httpMock.expectOne(service.endpoints.updateTask(1));
      expect(req.request.method).toBe('PUT');
      req.flush(response);
    });

    test('throws an error including response data on fail', () => {
      const response = {};

      service.updateTask({ id: 1, name: 'foo', done: false }).subscribe(
        () => {
          fail('expecting error');
        },
        err => {
          expect(err.error).toBe(response);
        },
      );

      const req = httpMock.expectOne(service.endpoints.updateTask(1));
      expect(req.request.method).toBe('PUT');
      req.flush(response, {
        status: 400,
        statusText: 'Bad request',
      });
    });
  });

  describe('#removeTask', () => {
    test('returns an observable of response data on success', () => {
      const response = {} as any;

      service.removeTask({ id: 1 }).subscribe(res => {
        expect(res).toBe(response);
      });

      const req = httpMock.expectOne(service.endpoints.removeTask(1));
      expect(req.request.method).toBe('DELETE');
      req.flush(response);
    });

    test('throws an error including response data on fail', () => {
      const response = {};

      service.removeTask({ id: 1 }).subscribe(
        () => {
          fail('expecting error');
        },
        err => {
          expect(err.error).toBe(response);
        },
      );

      const req = httpMock.expectOne(service.endpoints.removeTask(1));
      expect(req.request.method).toBe('DELETE');
      req.flush(response, {
        status: 400,
        statusText: 'Bad request',
      });
    });
  });
});

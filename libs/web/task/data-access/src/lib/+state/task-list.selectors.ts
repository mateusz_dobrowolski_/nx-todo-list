import { createFeatureSelector, createSelector } from '@ngrx/store';
import { TASKLIST_FEATURE_KEY, TaskListState } from './task-list.reducer';

// Lookup the 'TaskList' feature state managed by NgRx
const getTaskListState = createFeatureSelector<TaskListState>(
  TASKLIST_FEATURE_KEY,
);

const getTask = createSelector(getTaskListState, state => state.task);

const getTaskLoading = createSelector(
  getTaskListState,
  state => state.taskLoading,
);

const getTaskLoadError = createSelector(
  getTaskListState,
  state => state.taskLoadError,
);

const getTaskCollection = createSelector(
  getTaskListState,
  state => state.taskCollection,
);

const getTaskCollectionLoading = createSelector(
  getTaskListState,
  state => state.taskCollectionLoading,
);

const getTaskCollectionLoadError = createSelector(
  getTaskListState,
  state => state.taskCollectionLoadError,
);

const getTaskCreating = createSelector(
  getTaskListState,
  state => state.taskCreating,
);

const getTaskCreateError = createSelector(
  getTaskListState,
  state => state.taskCreateError,
);

const getTaskUpdating = createSelector(
  getTaskListState,
  state => state.taskUpdating,
);

const getTaskUpdateError = createSelector(
  getTaskListState,
  state => state.taskUpdateError,
);

const getTaskRemoving = createSelector(
  getTaskListState,
  state => state.taskRemoving,
);

const getTaskRemoveError = createSelector(
  getTaskListState,
  state => state.taskRemoveError,
);

const getDoneTasksCollection = createSelector(getTaskCollection, tasks =>
  tasks.filter(task => task.done),
);

const getToDoTasksCollection = createSelector(getTaskCollection, tasks =>
  tasks.filter(task => !task.done),
);

export const taskListQuery = {
  getTask,
  getTaskLoading,
  getTaskLoadError,
  getTaskCollection,
  getTaskCollectionLoading,
  getTaskCollectionLoadError,
  getTaskCreating,
  getTaskCreateError,
  getTaskUpdating,
  getTaskUpdateError,
  getTaskRemoving,
  getTaskRemoveError,
  getDoneTasksCollection,
  getToDoTasksCollection,
};

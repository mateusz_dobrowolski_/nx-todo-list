import { fromTaskListActions } from './task-list.actions';
import { HttpErrorResponse } from '@angular/common/http';
import { Task } from '@mateusz-d-todo-list/web/task/domain';

export const TASKLIST_FEATURE_KEY = 'taskList';

export interface TaskListState {
  task: Task | null;
  taskLoading: boolean;
  taskLoadError: HttpErrorResponse | null;
  taskCollection: Task[];
  taskCollectionLoading: boolean;
  taskCollectionLoadError: HttpErrorResponse | null;
  taskCreating: boolean;
  taskCreateError: HttpErrorResponse | null;
  taskUpdating: boolean;
  taskUpdateError: HttpErrorResponse | null;
  taskRemoving: boolean;
  taskRemoveError: HttpErrorResponse | null;
}

export interface TaskListPartialState {
  readonly [TASKLIST_FEATURE_KEY]: TaskListState;
}

export const initialState: TaskListState = {
  task: null,
  taskLoading: false,
  taskLoadError: null,
  taskCollection: [],
  taskCollectionLoading: false,
  taskCollectionLoadError: null,
  taskCreating: false,
  taskCreateError: null,
  taskUpdating: false,
  taskUpdateError: null,
  taskRemoving: false,
  taskRemoveError: null,
};

export function taskListReducer(
  state: TaskListState = initialState,
  action: fromTaskListActions.CollectiveType,
): TaskListState {
  switch (action.type) {
    case fromTaskListActions.Types.GetTask: {
      state = {
        ...state,
        task: null,
        taskLoading: true,
        taskLoadError: null,
      };
      break;
    }

    case fromTaskListActions.Types.GetTaskFail: {
      state = {
        ...state,
        task: null,
        taskLoading: false,
        taskLoadError: action.payload,
      };
      break;
    }

    case fromTaskListActions.Types.GetTaskSuccess: {
      state = {
        ...state,
        task: action.payload,
        taskLoading: false,
        taskLoadError: null,
      };
      break;
    }

    case fromTaskListActions.Types.GetTaskCollection: {
      state = {
        ...state,
        taskCollection: [],
        taskCollectionLoading: true,
        taskCollectionLoadError: null,
      };
      break;
    }

    case fromTaskListActions.Types.GetTaskCollectionFail: {
      state = {
        ...state,
        taskCollection: [],
        taskCollectionLoading: false,
        taskCollectionLoadError: action.payload,
      };
      break;
    }

    case fromTaskListActions.Types.GetTaskCollectionSuccess: {
      state = {
        ...state,
        taskCollection: action.payload,
        taskCollectionLoading: false,
        taskCollectionLoadError: null,
      };
      break;
    }

    case fromTaskListActions.Types.CreateTask: {
      state = {
        ...state,
        taskCreating: true,
        taskCreateError: null,
      };
      break;
    }

    case fromTaskListActions.Types.CreateTaskFail: {
      state = {
        ...state,
        taskCreating: false,
        taskCreateError: action.payload,
      };
      break;
    }

    case fromTaskListActions.Types.CreateTaskSuccess: {
      state = {
        ...state,
        taskCollection: [...state.taskCollection, action.payload],
        taskCreating: false,
        taskCreateError: null,
      };
      break;
    }

    case fromTaskListActions.Types.UpdateTask: {
      state = {
        ...state,
        taskUpdating: true,
        taskUpdateError: null,
      };
      break;
    }

    case fromTaskListActions.Types.UpdateTaskFail: {
      state = {
        ...state,
        taskUpdating: false,
        taskUpdateError: action.payload,
      };
      break;
    }

    case fromTaskListActions.Types.UpdateTaskSuccess: {
      state = {
        ...state,
        taskCollection: state.taskCollection.map(e =>
          e.id === action.payload.id ? action.payload : e,
        ),
        taskUpdating: false,
        taskUpdateError: null,
      };
      break;
    }

    case fromTaskListActions.Types.RemoveTask: {
      state = {
        ...state,
        taskRemoving: true,
        taskRemoveError: null,
      };
      break;
    }

    case fromTaskListActions.Types.RemoveTaskFail: {
      state = {
        ...state,
        taskRemoving: false,
        taskRemoveError: action.payload,
      };
      break;
    }

    case fromTaskListActions.Types.RemoveTaskSuccess: {
      state = {
        ...state,
        taskCollection: state.taskCollection.filter(
          e => e.id !== action.payload.id,
        ),
        taskRemoving: false,
        taskRemoveError: null,
      };
      break;
    }
  }

  return state;
}

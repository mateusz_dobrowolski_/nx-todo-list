import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';
import { fromTaskListActions } from './task-list.actions';
import { TaskListPartialState } from './task-list.reducer';
import { TaskListDataService } from '../services/task-list-data.service';
import { HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class TaskListEffects {
  @Effect()
  getTask$ = this.dp.fetch(fromTaskListActions.Types.GetTask, {
    id: action => action.payload.id,
    run: (action: fromTaskListActions.GetTask) => {
      return this.taskListDataService
        .getTask(action.payload)
        .pipe(map(data => new fromTaskListActions.GetTaskSuccess(data)));
    },
    onError: (
      action: fromTaskListActions.GetTask,
      error: HttpErrorResponse,
    ) => {
      return new fromTaskListActions.GetTaskFail(error);
    },
  });

  @Effect()
  getTaskCollection$ = this.dp.fetch(
    fromTaskListActions.Types.GetTaskCollection,
    {
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      id: () => {},
      run: () => {
        return this.taskListDataService
          .getTaskCollection()
          .pipe(
            map(data => new fromTaskListActions.GetTaskCollectionSuccess(data)),
          );
      },
      onError: (
        action: fromTaskListActions.GetTaskCollection,
        error: HttpErrorResponse,
      ) => {
        return new fromTaskListActions.GetTaskCollectionFail(error);
      },
    },
  );

  @Effect()
  createTask$ = this.dp.pessimisticUpdate(
    fromTaskListActions.Types.CreateTask,
    {
      run: (action: fromTaskListActions.CreateTask) => {
        return this.taskListDataService
          .createTask(action.payload)
          .pipe(map(data => new fromTaskListActions.CreateTaskSuccess(data)));
      },
      onError: (
        action: fromTaskListActions.CreateTask,
        error: HttpErrorResponse,
      ) => {
        return new fromTaskListActions.CreateTaskFail(error);
      },
    },
  );

  @Effect()
  updateTask$ = this.dp.pessimisticUpdate(
    fromTaskListActions.Types.UpdateTask,
    {
      run: (action: fromTaskListActions.UpdateTask) => {
        return this.taskListDataService
          .updateTask(action.payload)
          .pipe(map(data => new fromTaskListActions.UpdateTaskSuccess(data)));
      },
      onError: (
        action: fromTaskListActions.UpdateTask,
        error: HttpErrorResponse,
      ) => {
        return new fromTaskListActions.UpdateTaskFail(error);
      },
    },
  );

  @Effect()
  removeTask$ = this.dp.pessimisticUpdate(
    fromTaskListActions.Types.RemoveTask,
    {
      run: (action: fromTaskListActions.RemoveTask) => {
        return this.taskListDataService
          .removeTask(action.payload)
          .pipe(
            map(
              data => new fromTaskListActions.RemoveTaskSuccess(action.payload),
            ),
          );
      },
      onError: (
        action: fromTaskListActions.RemoveTask,
        error: HttpErrorResponse,
      ) => {
        return new fromTaskListActions.RemoveTaskFail(error);
      },
    },
  );

  constructor(
    private dp: DataPersistence<TaskListPartialState>,
    private taskListDataService: TaskListDataService,
  ) {}
}

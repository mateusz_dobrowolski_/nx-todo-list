import { Action } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import {
  GetTaskPayload,
  RemoveTaskPayload,
  Task,
  TaskTitlePayload,
} from '@mateusz-d-todo-list/web/task/domain';

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace fromTaskListActions {
  export enum Types {
    GetTask = '[Tasks] Get Task',
    GetTaskFail = '[Tasks] Get Task Fail',
    GetTaskSuccess = '[Tasks] Get Task Success',
    GetTaskCollection = '[Tasks] Get Task Collection',
    GetTaskCollectionFail = '[Tasks] Get Task Collection Fail',
    GetTaskCollectionSuccess = '[Tasks] Get Task Collection Success',
    CreateTask = '[Tasks] Create Task',
    CreateTaskFail = '[Tasks] Create Task Fail',
    CreateTaskSuccess = '[Tasks] Create Task Success',
    UpdateTask = '[Tasks] Update Task',
    UpdateTaskFail = '[Tasks] Update Task Fail',
    UpdateTaskSuccess = '[Tasks] Update Task Success',
    RemoveTask = '[Tasks] Remove Task',
    RemoveTaskFail = '[Tasks] Remove Task Fail',
    RemoveTaskSuccess = '[Tasks] Remove Task Success',
  }

  export class GetTask implements Action {
    readonly type = Types.GetTask;

    constructor(public payload: GetTaskPayload) {}
  }

  export class GetTaskFail implements Action {
    readonly type = Types.GetTaskFail;

    constructor(public payload: HttpErrorResponse) {}
  }

  export class GetTaskSuccess implements Action {
    readonly type = Types.GetTaskSuccess;

    constructor(public payload: Task) {}
  }

  export class GetTaskCollection implements Action {
    readonly type = Types.GetTaskCollection;
  }

  export class GetTaskCollectionFail implements Action {
    readonly type = Types.GetTaskCollectionFail;

    constructor(public payload: HttpErrorResponse) {}
  }

  export class GetTaskCollectionSuccess implements Action {
    readonly type = Types.GetTaskCollectionSuccess;

    constructor(public payload: Task[]) {}
  }

  export class CreateTask implements Action {
    readonly type = Types.CreateTask;

    constructor(public payload: TaskTitlePayload) {}
  }

  export class CreateTaskFail implements Action {
    readonly type = Types.CreateTaskFail;

    constructor(public payload: HttpErrorResponse) {}
  }

  export class CreateTaskSuccess implements Action {
    readonly type = Types.CreateTaskSuccess;

    constructor(public payload: Task) {}
  }

  export class UpdateTask implements Action {
    readonly type = Types.UpdateTask;

    constructor(public payload: Task) {}
  }

  export class UpdateTaskFail implements Action {
    readonly type = Types.UpdateTaskFail;

    constructor(public payload: HttpErrorResponse) {}
  }

  export class UpdateTaskSuccess implements Action {
    readonly type = Types.UpdateTaskSuccess;

    constructor(public payload: Task) {}
  }

  export class RemoveTask implements Action {
    readonly type = Types.RemoveTask;

    constructor(public payload: RemoveTaskPayload) {}
  }

  export class RemoveTaskFail implements Action {
    readonly type = Types.RemoveTaskFail;

    constructor(public payload: HttpErrorResponse) {}
  }

  export class RemoveTaskSuccess implements Action {
    readonly type = Types.RemoveTaskSuccess;

    constructor(public payload: RemoveTaskPayload) {}
  }

  export type CollectiveType =
    | GetTask
    | GetTaskFail
    | GetTaskSuccess
    | GetTaskCollection
    | GetTaskCollectionFail
    | GetTaskCollectionSuccess
    | CreateTask
    | CreateTaskFail
    | CreateTaskSuccess
    | UpdateTask
    | UpdateTaskFail
    | UpdateTaskSuccess
    | RemoveTask
    | RemoveTaskFail
    | RemoveTaskSuccess;
}

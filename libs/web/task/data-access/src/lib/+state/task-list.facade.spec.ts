import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { NxModule } from '@nrwl/angular';
import { Observable } from 'rxjs';
import { TaskListFacade } from './task-list.facade';
import { fromTaskListActions } from './task-list.actions';

describe('TaskListFacade', () => {
  let actions: Observable<any>;
  let facade: TaskListFacade;
  let store: MockStore;

  describe('used in NgModule', () => {
    beforeEach(() => {
      @NgModule({
        imports: [],
        providers: [
          TaskListFacade,
          provideMockStore(),
          provideMockActions(() => actions),
        ],
      })
      class CustomFeatureModule {}

      @NgModule({
        imports: [NxModule.forRoot(), CustomFeatureModule],
      })
      class RootModule {}

      TestBed.configureTestingModule({ imports: [RootModule] });
      facade = TestBed.inject(TaskListFacade);
      store = TestBed.inject(MockStore);
      jest.spyOn(store, 'dispatch');
    });

    describe('#getTask', () => {
      test('should dispatch fromTaskListActions.GetTask action', () => {
        const payload = {} as any;
        const action = new fromTaskListActions.GetTask(payload);

        facade.getTask(payload);
        expect(store.dispatch).toHaveBeenCalledWith(action);
      });
    });

    describe('#getTaskCollection', () => {
      test('should dispatch fromTaskListActions.GetTaskCollection action', () => {
        const action = new fromTaskListActions.GetTaskCollection();

        facade.getTaskCollection();
        expect(store.dispatch).toHaveBeenCalledWith(action);
      });
    });

    describe('#createTask', () => {
      test('should dispatch fromTaskListActions.CreateTask action', () => {
        const payload = {} as any;
        const action = new fromTaskListActions.CreateTask(payload);

        facade.createTask(payload);
        expect(store.dispatch).toHaveBeenCalledWith(action);
      });
    });

    describe('#updateTask', () => {
      test('should dispatch fromTaskListActions.UpdateTask action', () => {
        const payload = {} as any;
        const action = new fromTaskListActions.UpdateTask(payload);

        facade.updateTask(payload);
        expect(store.dispatch).toHaveBeenCalledWith(action);
      });
    });

    describe('#removeTask', () => {
      test('should dispatch fromTaskListActions.RemoveTask action', () => {
        const payload = {} as any;
        const action = new fromTaskListActions.RemoveTask(payload);

        facade.removeTask(payload);
        expect(store.dispatch).toHaveBeenCalledWith(action);
      });
    });
  });
});

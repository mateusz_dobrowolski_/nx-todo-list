import { TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { DataPersistence, NxModule } from '@nrwl/angular';
import { cold, hot } from 'jest-marbles';
import { TaskListEffects } from './task-list.effects';
import { fromTaskListActions } from './task-list.actions';
import { TaskListDataService } from '../services/task-list-data.service';
import { createSpyObj } from 'jest-createspyobj';

describe('TaskListEffects', () => {
  let taskListDataService: jest.Mocked<TaskListDataService>;
  let actions: Observable<any>;
  let effects: TaskListEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NxModule.forRoot()],
      providers: [
        TaskListEffects,
        DataPersistence,
        provideMockActions(() => actions),
        provideMockStore({ initialState: {} }),
        {
          provide: TaskListDataService,
          useValue: createSpyObj(TaskListDataService),
        },
      ],
    });

    effects = TestBed.inject(TaskListEffects);
    taskListDataService = TestBed.inject(
      TaskListDataService,
    ) as jest.Mocked<TaskListDataService>;
  });

  describe('getTask$', () => {
    test('returns GetTaskSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromTaskListActions.GetTask({} as any);
      const completion = new fromTaskListActions.GetTaskSuccess(payload);

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      taskListDataService.getTask.mockReturnValue(response);

      expect(effects.getTask$).toSatisfyOnFlush(() => {
        expect(taskListDataService.getTask).toHaveBeenCalled();
      });
      expect(effects.getTask$).toBeObservable(expected);
    });

    test('returns GetTaskFail action on fail', () => {
      const payload = {} as any;
      const action = new fromTaskListActions.GetTask({} as any);
      const completion = new fromTaskListActions.GetTaskFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      taskListDataService.getTask.mockReturnValue(response);

      expect(effects.getTask$).toSatisfyOnFlush(() => {
        expect(taskListDataService.getTask).toHaveBeenCalled();
      });
      expect(effects.getTask$).toBeObservable(expected);
    });
  });

  describe('getTaskCollection$', () => {
    test('returns GetTaskCollectionSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromTaskListActions.GetTaskCollection();
      const completion = new fromTaskListActions.GetTaskCollectionSuccess(
        payload,
      );

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      taskListDataService.getTaskCollection.mockReturnValue(response);

      expect(effects.getTaskCollection$).toSatisfyOnFlush(() => {
        expect(taskListDataService.getTaskCollection).toHaveBeenCalled();
      });
      expect(effects.getTaskCollection$).toBeObservable(expected);
    });

    test('returns GetTaskCollectionFail action on fail', () => {
      const payload = {} as any;
      const action = new fromTaskListActions.GetTaskCollection();
      const completion = new fromTaskListActions.GetTaskCollectionFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      taskListDataService.getTaskCollection.mockReturnValue(response);

      expect(effects.getTaskCollection$).toSatisfyOnFlush(() => {
        expect(taskListDataService.getTaskCollection).toHaveBeenCalled();
      });
      expect(effects.getTaskCollection$).toBeObservable(expected);
    });
  });

  describe('createTask$', () => {
    test('returns CreateTaskSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromTaskListActions.CreateTask({} as any);
      const completion = new fromTaskListActions.CreateTaskSuccess(payload);

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      taskListDataService.createTask.mockReturnValue(response);

      expect(effects.createTask$).toSatisfyOnFlush(() => {
        expect(taskListDataService.createTask).toHaveBeenCalled();
      });
      expect(effects.createTask$).toBeObservable(expected);
    });

    test('returns CreateTaskFail action on fail', () => {
      const payload = {} as any;
      const action = new fromTaskListActions.CreateTask({} as any);
      const completion = new fromTaskListActions.CreateTaskFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      taskListDataService.createTask.mockReturnValue(response);

      expect(effects.createTask$).toSatisfyOnFlush(() => {
        expect(taskListDataService.createTask).toHaveBeenCalled();
      });
      expect(effects.createTask$).toBeObservable(expected);
    });
  });

  describe('updateTask$', () => {
    test('returns UpdateTaskSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromTaskListActions.UpdateTask({} as any);
      const completion = new fromTaskListActions.UpdateTaskSuccess(payload);

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      taskListDataService.updateTask.mockReturnValue(response);

      expect(effects.updateTask$).toSatisfyOnFlush(() => {
        expect(taskListDataService.updateTask).toHaveBeenCalled();
      });
      expect(effects.updateTask$).toBeObservable(expected);
    });

    test('returns UpdateTaskFail action on fail', () => {
      const payload = {} as any;
      const action = new fromTaskListActions.UpdateTask({} as any);
      const completion = new fromTaskListActions.UpdateTaskFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      taskListDataService.updateTask.mockReturnValue(response);

      expect(effects.updateTask$).toSatisfyOnFlush(() => {
        expect(taskListDataService.updateTask).toHaveBeenCalled();
      });
      expect(effects.updateTask$).toBeObservable(expected);
    });
  });

  describe('removeTask$', () => {
    test('returns RemoveTaskSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromTaskListActions.RemoveTask({} as any);
      const completion = new fromTaskListActions.RemoveTaskSuccess(payload);

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      taskListDataService.removeTask.mockReturnValue(response);

      expect(effects.removeTask$).toSatisfyOnFlush(() => {
        expect(taskListDataService.removeTask).toHaveBeenCalled();
      });
      expect(effects.removeTask$).toBeObservable(expected);
    });

    test('returns RemoveTaskFail action on fail', () => {
      const payload = {} as any;
      const action = new fromTaskListActions.RemoveTask({} as any);
      const completion = new fromTaskListActions.RemoveTaskFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      taskListDataService.removeTask.mockReturnValue(response);

      expect(effects.removeTask$).toSatisfyOnFlush(() => {
        expect(taskListDataService.removeTask).toHaveBeenCalled();
      });
      expect(effects.removeTask$).toBeObservable(expected);
    });
  });
});

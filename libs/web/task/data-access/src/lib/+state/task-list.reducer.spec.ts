import { fromTaskListActions } from './task-list.actions';
import {
  initialState,
  taskListReducer,
  TaskListState,
} from './task-list.reducer';
import { statesEqual } from '@valueadd/testing';

describe('TaskList Reducer', () => {
  let state: TaskListState;

  beforeEach(() => {
    state = { ...initialState };
  });

  describe('unknown action', () => {
    test('returns the initial state', () => {
      const action = {} as any;
      const result = taskListReducer(state, action);

      expect(result).toBe(state);
    });
  });

  describe('GetTask', () => {
    test('sets task, taskLoading, taskLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromTaskListActions.GetTask(payload);
      const result = taskListReducer(state, action);

      expect(result.task).toEqual(null);
      expect(result.taskLoading).toEqual(true);
      expect(result.taskLoadError).toEqual(null);
      expect(
        statesEqual(result, state, ['task', 'taskLoading', 'taskLoadError']),
      ).toBeTruthy();
    });
  });

  describe('GetTaskFail', () => {
    test('sets task, taskLoading, taskLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromTaskListActions.GetTaskFail(payload);
      const result = taskListReducer(state, action);

      expect(result.task).toEqual(null);
      expect(result.taskLoading).toEqual(false);
      expect(result.taskLoadError).toEqual(payload);
      expect(
        statesEqual(result, state, ['task', 'taskLoading', 'taskLoadError']),
      ).toBeTruthy();
    });
  });

  describe('GetTaskSuccess', () => {
    test('sets task, taskLoading, taskLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromTaskListActions.GetTaskSuccess(payload);
      const result = taskListReducer(state, action);

      expect(result.task).toEqual(payload);
      expect(result.taskLoading).toEqual(false);
      expect(result.taskLoadError).toEqual(null);
      expect(
        statesEqual(result, state, ['task', 'taskLoading', 'taskLoadError']),
      ).toBeTruthy();
    });
  });

  describe('GetTaskCollection', () => {
    test('sets taskCollection, taskCollectionLoading, taskCollectionLoadError and does not modify other state properties', () => {
      const action = new fromTaskListActions.GetTaskCollection();
      const result = taskListReducer(state, action);

      expect(result.taskCollection).toEqual([]);
      expect(result.taskCollectionLoading).toEqual(true);
      expect(result.taskCollectionLoadError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'taskCollection',
          'taskCollectionLoading',
          'taskCollectionLoadError',
        ]),
      ).toBeTruthy();
    });
  });

  describe('GetTaskCollectionFail', () => {
    test('sets taskCollection, taskCollectionLoading, taskCollectionLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromTaskListActions.GetTaskCollectionFail(payload);
      const result = taskListReducer(state, action);

      expect(result.taskCollection).toEqual([]);
      expect(result.taskCollectionLoading).toEqual(false);
      expect(result.taskCollectionLoadError).toEqual(payload);
      expect(
        statesEqual(result, state, [
          'taskCollection',
          'taskCollectionLoading',
          'taskCollectionLoadError',
        ]),
      ).toBeTruthy();
    });
  });

  describe('GetTaskCollectionSuccess', () => {
    test('sets taskCollection, taskCollectionLoading, taskCollectionLoadError and does not modify other state properties', () => {
      // payload nie powinien być pusty, bo używasz go w reducerze
      const payload = {} as any;
      const action = new fromTaskListActions.GetTaskCollectionSuccess(payload);
      const result = taskListReducer(state, action);

      expect(result.taskCollection).toEqual(payload);
      expect(result.taskCollectionLoading).toEqual(false);
      expect(result.taskCollectionLoadError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'taskCollection',
          'taskCollectionLoading',
          'taskCollectionLoadError',
        ]),
      ).toBeTruthy();
    });
  });

  describe('CreateTask', () => {
    test('sets taskCreating, taskCreateError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromTaskListActions.CreateTask(payload);
      const result = taskListReducer(state, action);

      expect(result.taskCreating).toEqual(true);
      expect(result.taskCreateError).toEqual(null);
      expect(
        statesEqual(result, state, ['taskCreating', 'taskCreateError']),
      ).toBeTruthy();
    });
  });

  describe('CreateTaskFail', () => {
    test('sets taskCreating, taskCreateError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromTaskListActions.CreateTaskFail(payload);
      const result = taskListReducer(state, action);

      expect(result.taskCreating).toEqual(false);
      expect(result.taskCreateError).toEqual(payload);
      expect(
        statesEqual(result, state, ['taskCreating', 'taskCreateError']),
      ).toBeTruthy();
    });
  });

  describe('CreateTaskSuccess', () => {
    test('sets taskCollection, taskCreating, taskCreateError and does not modify other state properties', () => {
      state = {
        ...state,
        taskCollection: [
          {
            id: 1,
            name: 'foo',
            done: false,
          },
        ],
      };

      const payload = { id: 2, name: 'bar', done: true };
      const action = new fromTaskListActions.CreateTaskSuccess(payload);
      const result = taskListReducer(state, action);

      expect(result.taskCollection.length).toEqual(2);
      expect(result.taskCreating).toEqual(false);
      expect(result.taskCreateError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'taskCollection',
          'taskCreating',
          'taskCreateError',
        ]),
      ).toBeTruthy();
    });
  });

  describe('UpdateTask', () => {
    test('sets taskUpdating, taskUpdateError and does not modify other state properties', () => {
      state = {
        ...state,
        taskCollection: [
          {
            id: 1,
            name: 'foo',
            done: false,
          },
        ],
      };

      const payload = { id: 1, name: 'bar', done: true };
      const action = new fromTaskListActions.UpdateTask(payload);
      const result = taskListReducer(state, action);

      expect(result.taskUpdating).toEqual(true);
      expect(result.taskUpdateError).toEqual(null);
      expect(
        statesEqual(result, state, ['taskUpdating', 'taskUpdateError']),
      ).toBeTruthy();
    });
  });

  describe('UpdateTaskFail', () => {
    test('sets taskUpdating, taskUpdateError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromTaskListActions.UpdateTaskFail(payload);
      const result = taskListReducer(state, action);

      expect(result.taskUpdating).toEqual(false);
      expect(result.taskUpdateError).toEqual(payload);
      expect(
        statesEqual(result, state, ['taskUpdating', 'taskUpdateError']),
      ).toBeTruthy();
    });
  });

  describe('UpdateTaskSuccess', () => {
    test('sets taskCollection, taskUpdating, taskUpdateError and does not modify other state properties', () => {
      state = {
        ...state,
        taskCollection: [
          {
            id: 1,
            name: 'foo',
            done: false,
          },
        ],
      };

      const payload = { id: 1, name: 'bar', done: true };

      const action = new fromTaskListActions.UpdateTaskSuccess(payload);
      const result = taskListReducer(state, action);

      expect(result.taskCollection.find(el => el.id === 1)?.name).toEqual(
        'bar',
      );
      expect((result as any).taskUpdating).toEqual(false);
      expect((result as any).taskUpdateError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'taskCollection',
          'taskUpdating',
          'taskUpdateError',
        ]),
      ).toBeTruthy();
    });
  });

  describe('RemoveTask', () => {
    test('sets taskRemoving, taskRemoveError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromTaskListActions.RemoveTask(payload);
      const result = taskListReducer(state, action);

      expect(result.taskRemoving).toEqual(true);
      expect(result.taskRemoveError).toEqual(null);
      expect(
        statesEqual(result, state, ['taskRemoving', 'taskRemoveError']),
      ).toBeTruthy();
    });
  });

  describe('RemoveTaskFail', () => {
    test('sets taskRemoving, taskRemoveError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromTaskListActions.RemoveTaskFail(payload);
      const result = taskListReducer(state, action);

      expect(result.taskRemoving).toEqual(false);
      expect(result.taskRemoveError).toEqual(payload);
      expect(
        statesEqual(result, state, ['taskRemoving', 'taskRemoveError']),
      ).toBeTruthy();
    });
  });

  describe('RemoveTaskSuccess', () => {
    test('sets taskCollection, taskRemoving, taskRemoveError and does not modify other state properties', () => {
      state = {
        ...initialState,
        taskCollection: [{ id: '1', name: 'test' } as any],
      };
      const payload = { id: '1', name: 'test2' } as any;
      const action = new fromTaskListActions.RemoveTaskSuccess(payload);
      const result = taskListReducer(state, action);

      expect(result.taskCollection.length).toEqual(0);
      expect(result.taskRemoving).toEqual(false);
      expect(result.taskRemoveError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'taskCollection',
          'taskRemoving',
          'taskRemoveError',
        ]),
      ).toBeTruthy();
    });
  });
});

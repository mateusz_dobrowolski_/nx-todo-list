import {
  initialState,
  TASKLIST_FEATURE_KEY,
  TaskListState,
} from './task-list.reducer';
import { taskListQuery } from './task-list.selectors';

describe('TaskList Selectors', () => {
  let storeState: { [TASKLIST_FEATURE_KEY]: TaskListState };

  beforeEach(() => {
    storeState = {
      [TASKLIST_FEATURE_KEY]: initialState,
    };
  });

  test('getTask() returns task value', () => {
    const result = taskListQuery.getTask(storeState);

    expect(result).toBe(storeState[TASKLIST_FEATURE_KEY].task);
  });

  test('getTaskLoading() returns taskLoading value', () => {
    const result = taskListQuery.getTaskLoading(storeState);

    expect(result).toBe(storeState[TASKLIST_FEATURE_KEY].taskLoading);
  });

  test('getTaskLoadError() returns taskLoadError value', () => {
    const result = taskListQuery.getTaskLoadError(storeState);

    expect(result).toBe(storeState[TASKLIST_FEATURE_KEY].taskLoadError);
  });

  test('getTaskCollection() returns taskCollection value', () => {
    const result = taskListQuery.getTaskCollection(storeState);

    expect(result).toBe(storeState[TASKLIST_FEATURE_KEY].taskCollection);
  });

  test('getTaskCollectionLoading() returns taskCollectionLoading value', () => {
    const result = taskListQuery.getTaskCollectionLoading(storeState);

    expect(result).toBe(storeState[TASKLIST_FEATURE_KEY].taskCollectionLoading);
  });

  test('getTaskCollectionLoadError() returns taskCollectionLoadError value', () => {
    const result = taskListQuery.getTaskCollectionLoadError(storeState);

    expect(result).toBe(
      storeState[TASKLIST_FEATURE_KEY].taskCollectionLoadError,
    );
  });

  test('getTaskCreating() returns taskCreating value', () => {
    const result = taskListQuery.getTaskCreating(storeState);

    expect(result).toBe(storeState[TASKLIST_FEATURE_KEY].taskCreating);
  });

  test('getTaskCreateError() returns taskCreateError value', () => {
    const result = taskListQuery.getTaskCreateError(storeState);

    expect(result).toBe(storeState[TASKLIST_FEATURE_KEY].taskCreateError);
  });

  test('getTaskUpdating() returns taskUpdating value', () => {
    const result = taskListQuery.getTaskUpdating(storeState);

    expect(result).toBe(storeState[TASKLIST_FEATURE_KEY].taskUpdating);
  });

  test('getTaskUpdateError() returns taskUpdateError value', () => {
    const result = taskListQuery.getTaskUpdateError(storeState);

    expect(result).toBe(storeState[TASKLIST_FEATURE_KEY].taskUpdateError);
  });

  test('getTaskRemoving() returns taskRemoving value', () => {
    const result = taskListQuery.getTaskRemoving(storeState);

    expect(result).toBe(storeState[TASKLIST_FEATURE_KEY].taskRemoving);
  });

  test('getTaskRemoveError() returns taskRemoveError value', () => {
    const result = taskListQuery.getTaskRemoveError(storeState);

    expect(result).toBe(storeState[TASKLIST_FEATURE_KEY].taskRemoveError);
  });
});

import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { TaskListPartialState } from './task-list.reducer';
import { taskListQuery } from './task-list.selectors';
import { fromTaskListActions } from './task-list.actions';
import {
  GetTaskPayload,
  RemoveTaskPayload,
  Task,
  TaskTitlePayload,
} from '@mateusz-d-todo-list/web/task/domain';

@Injectable()
export class TaskListFacade {
  task$ = this.store.pipe(select(taskListQuery.getTask));
  taskLoading$ = this.store.pipe(select(taskListQuery.getTaskLoading));
  taskLoadError$ = this.store.pipe(select(taskListQuery.getTaskLoadError));
  taskCollection$ = this.store.pipe(select(taskListQuery.getTaskCollection));
  taskToDoCollection$ = this.store.select(taskListQuery.getToDoTasksCollection);
  taskDoneCollection$ = this.store.select(taskListQuery.getDoneTasksCollection);
  taskCollectionLoading$ = this.store.pipe(
    select(taskListQuery.getTaskCollectionLoading),
  );
  taskCollectionLoadError$ = this.store.pipe(
    select(taskListQuery.getTaskCollectionLoadError),
  );
  taskCreating$ = this.store.pipe(select(taskListQuery.getTaskCreating));
  taskCreateError$ = this.store.pipe(select(taskListQuery.getTaskCreateError));
  taskUpdating$ = this.store.pipe(select(taskListQuery.getTaskUpdating));
  taskUpdateError$ = this.store.pipe(select(taskListQuery.getTaskUpdateError));
  taskRemoving$ = this.store.pipe(select(taskListQuery.getTaskRemoving));
  taskRemoveError$ = this.store.pipe(select(taskListQuery.getTaskRemoveError));

  constructor(private store: Store<TaskListPartialState>) {}

  getTask(data: GetTaskPayload): void {
    this.store.dispatch(new fromTaskListActions.GetTask(data));
  }

  getTaskCollection(): void {
    this.store.dispatch(new fromTaskListActions.GetTaskCollection());
  }

  createTask(data: TaskTitlePayload): void {
    this.store.dispatch(new fromTaskListActions.CreateTask(data));
  }

  updateTask(data: Task): void {
    this.store.dispatch(new fromTaskListActions.UpdateTask(data));
  }

  removeTask(data: RemoveTaskPayload): void {
    this.store.dispatch(new fromTaskListActions.RemoveTask(data));
  }
}

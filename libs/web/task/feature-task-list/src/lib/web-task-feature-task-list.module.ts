import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskListContainerComponent } from './containers/task-list-container/task-list-container.component';
import { WebTaskFeatureTaskListRoutingModule } from './web-task-feature-task-list-routing.module';
import { WebTaskUiTaskListModule } from '@mateusz-d-todo-list/web/task/ui-task-list';
import { WebTaskDataAccessModule } from '@mateusz-d-todo-list/web/task/data-access';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { WebTaskUiTaskFormModule } from '@mateusz-d-todo-list/web/task/ui-task-form';

@NgModule({
  imports: [
    CommonModule,
    WebTaskFeatureTaskListRoutingModule,
    WebTaskUiTaskListModule,
    WebTaskDataAccessModule,
    MatProgressSpinnerModule,
    WebTaskUiTaskFormModule,
  ],
  declarations: [TaskListContainerComponent],
})
export class WebTaskFeatureTaskListModule {}

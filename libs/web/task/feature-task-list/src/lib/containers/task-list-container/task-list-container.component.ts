import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Task, TaskTitlePayload } from '@mateusz-d-todo-list/web/task/domain';
import { TaskListFacade } from '@mateusz-d-todo-list/web/task/data-access';
import { Observable } from 'rxjs';

@Component({
  selector: 'mateusz-d-todo-list-task-list-container',
  templateUrl: './task-list-container.component.html',
  styleUrls: ['./task-list-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaskListContainerComponent implements OnInit {
  readonly taskTodoCollection$: Observable<Task[]> = this.taskListFacade
    .taskToDoCollection$;
  readonly taskDoneCollection$: Observable<Task[]> = this.taskListFacade
    .taskDoneCollection$;
  readonly taskCollectionLoading$: Observable<boolean> = this.taskListFacade
    .taskCollectionLoading$;
  readonly taskCreating$: Observable<boolean> = this.taskListFacade
    .taskCreating$;

  constructor(private readonly taskListFacade: TaskListFacade) {}

  ngOnInit(): void {
    this.taskListFacade.getTaskCollection();
  }

  onMarkedAsDone(task: Task): void {
    this.taskListFacade.updateTask({
      ...task,
      done: true,
    });
  }

  onMarkedAsTodo(task: Task): void {
    this.taskListFacade.updateTask({
      ...task,
      done: false,
    });
  }

  newTaskHandler(data: TaskTitlePayload): void {
    this.taskListFacade.createTask(data);
  }

  onRemove(data: Task): void {
    this.taskListFacade.removeTask({ id: data.id });
  }

  onUpdate(data: Task): void {
    this.taskListFacade.updateTask(data);
  }
}

import { NgModule } from '@angular/core';
import { TaskListContainerComponent } from './containers/task-list-container/task-list-container.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: TaskListContainerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WebTaskFeatureTaskListRoutingModule {}

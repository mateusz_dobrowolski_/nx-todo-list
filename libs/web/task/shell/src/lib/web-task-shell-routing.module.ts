import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: 'tasks',
    loadChildren: () =>
      import('@mateusz-d-todo-list/web/task/feature-task-list').then(
        m => m.WebTaskFeatureTaskListModule,
      ),
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WebTaskShellRoutingModule {}

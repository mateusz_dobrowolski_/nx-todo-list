import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebTaskShellRoutingModule } from './web-task-shell-routing.module';

@NgModule({
  imports: [CommonModule, WebTaskShellRoutingModule],
})
export class WebTaskShellModule {}

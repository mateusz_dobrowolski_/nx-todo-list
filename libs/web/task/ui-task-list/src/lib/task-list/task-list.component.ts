import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { Task, TaskTitlePayload } from '@mateusz-d-todo-list/web/task/domain';

@Component({
  selector: 'mateusz-d-todo-list-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaskListComponent {
  @Input() tasks: Task[] | null;
  @Output() markedAsDone = new EventEmitter<Task>();
  @Output() markedAsToDo = new EventEmitter<Task>();
  @Output() removed = new EventEmitter<Task>();
  @Output() updated = new EventEmitter<Task>();

  onMarkedAsDone(task: Task): void {
    this.markedAsDone.emit(task);
  }

  onMarkedAsTodo(task: Task): void {
    this.markedAsToDo.emit(task);
  }

  onRemove(task: Task): void {
    this.removed.emit(task);
  }

  onUpdate(task: Task, data: TaskTitlePayload): void {
    this.updated.emit({
      ...task,
      ...data,
    });
  }
}

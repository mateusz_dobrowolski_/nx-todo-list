import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskListComponent } from './task-list/task-list.component';
import { WebTaskUiTaskCardModule } from '@mateusz-d-todo-list/web/task/ui-task-card';

@NgModule({
  imports: [CommonModule, WebTaskUiTaskCardModule],
  declarations: [TaskListComponent],
  exports: [TaskListComponent],
})
export class WebTaskUiTaskListModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskFormComponent } from './task-form/task-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, MatInputModule, MatButtonModule],
  declarations: [TaskFormComponent],
  exports: [TaskFormComponent],
})
export class WebTaskUiTaskFormModule {}

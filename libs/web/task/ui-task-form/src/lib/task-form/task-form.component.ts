import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TaskTitlePayload } from '@mateusz-d-todo-list/web/task/domain';

@Component({
  selector: 'mateusz-d-todo-list-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaskFormComponent implements OnInit {
  @Input() loading: boolean | null;
  @Output() submitted = new EventEmitter<TaskTitlePayload>();

  formGroup: FormGroup;

  constructor(private readonly formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.initForm();
  }

  onSubmit(): void {
    if (this.formGroup.valid && !this.loading) {
      const formValue: TaskTitlePayload = { ...this.formGroup.value };
      this.submitted.emit(formValue);
      this.formGroup.reset();
    } else {
      this.formGroup.markAllAsTouched();
    }
  }

  private initForm(): void {
    this.formGroup = this.formBuilder.group(
      {
        name: [null, [Validators.required]],
      },
      {
        updateOn: 'submit',
      },
    );
  }
}

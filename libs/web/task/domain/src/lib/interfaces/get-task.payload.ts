export interface GetTaskPayload {
  id: string | number;
}

export interface RemoveTaskPayload {
  id: string | number;
}

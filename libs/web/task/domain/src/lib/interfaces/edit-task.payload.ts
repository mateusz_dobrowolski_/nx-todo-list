import { Task } from './task.interface';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface EditTaskPayload extends Omit<Task, 'id'> {}

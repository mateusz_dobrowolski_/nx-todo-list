import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebAuthShellRoutingModule } from './web-auth-shell-routing.module';

@NgModule({
  imports: [CommonModule, WebAuthShellRoutingModule],
})
export class WebAuthShellModule {}

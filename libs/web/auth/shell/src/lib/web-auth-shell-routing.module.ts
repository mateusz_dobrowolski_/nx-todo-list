import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'auth',
    children: [
      {
        path: 'login',
        loadChildren: () =>
          import('@mateusz-d-todo-list/web/auth/feature-login').then(
            m => m.WebAuthFeatureLoginModule,
          ),
      },
      {
        path: 'sign-up',
        loadChildren: () =>
          import('@mateusz-d-todo-list/web/auth/feature-sign-up').then(
            m => m.WebAuthFeatureSignUpModule,
          ),
      },
    ],
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/auth/login',
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WebAuthShellRoutingModule {}

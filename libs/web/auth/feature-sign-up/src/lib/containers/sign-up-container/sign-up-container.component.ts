import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AuthFacade } from '@mateusz-d-todo-list/web/auth/data-access';
import { SignUpPayload } from '../../../../../domain/src/lib/payloads/sign-up.payload';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'mateusz-d-todo-list-sign-up-container',
  templateUrl: './sign-up-container.component.html',
  styleUrls: ['./sign-up-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignUpContainerComponent {
  constructor(
    private readonly authFacade: AuthFacade,
    private readonly snackbarService: MatSnackBar,
  ) {}

  onSignUp(payload: SignUpPayload): void {
    this.authFacade.signUp(payload).subscribe({
      next: () => this.signUpSuccessHandler(),
      error: (error: unknown) => this.signUpFailHandler(error),
    });
  }

  private signUpSuccessHandler(): void {
    this.snackbarService.open('Sign-up success');
  }

  private signUpFailHandler(error: unknown): void {
    this.snackbarService.open('Sign-up fail');
  }
}

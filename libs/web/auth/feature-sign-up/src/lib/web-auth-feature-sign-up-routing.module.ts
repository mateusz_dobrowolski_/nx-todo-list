import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SignUpContainerComponent } from './containers/sign-up-container/sign-up-container.component';

const routes: Routes = [
  {
    path: '',
    component: SignUpContainerComponent,
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WebAuthFeatureSignUpRoutingModule {}

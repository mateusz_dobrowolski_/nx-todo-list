import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebAuthFeatureSignUpRoutingModule } from './web-auth-feature-sign-up-routing.module';
import { SignUpContainerComponent } from './containers/sign-up-container/sign-up-container.component';
import { WebAuthUiSignUpFormModule } from '@mateusz-d-todo-list/web/auth/ui-sign-up-form';
import { WebAuthDataAccessModule } from '@mateusz-d-todo-list/web/auth/data-access';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
  imports: [
    CommonModule,
    WebAuthFeatureSignUpRoutingModule,
    WebAuthUiSignUpFormModule,
    WebAuthDataAccessModule,
    MatSnackBarModule,
  ],
  declarations: [SignUpContainerComponent],
})
export class WebAuthFeatureSignUpModule {}

import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnInit,
  Output,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SignUpPayload } from '../../../../domain/src/lib/payloads/sign-up.payload';

@Component({
  selector: 'mateusz-d-todo-list-sign-up-form',
  templateUrl: './sign-up-form.component.html',
  styleUrls: ['./sign-up-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignUpFormComponent implements OnInit {
  formGroup: FormGroup;
  @Output() signUp = new EventEmitter<SignUpPayload>();

  constructor(private readonly formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.initFormGroup();
  }

  onSubmit(): void {
    if (this.formGroup.valid) {
      this.signUp.emit({
        ...this.formGroup.value,
      });
    }
  }

  private initFormGroup(): void {
    this.formGroup = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
    });
  }
}

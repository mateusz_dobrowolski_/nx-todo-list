import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnInit,
  Output,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginPayload } from '../../../../domain/src/lib/payloads/login.payload';

@Component({
  selector: 'mateusz-d-todo-list-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginFormComponent implements OnInit {
  formGroup: FormGroup;
  @Output() login = new EventEmitter<LoginPayload>();

  constructor(private readonly formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.initFormGroup();
  }

  onSubmit(): void {
    if (this.formGroup.valid) {
      this.login.emit({
        ...this.formGroup.value,
      });
    }
  }

  private initFormGroup(): void {
    this.formGroup = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
    });
  }
}

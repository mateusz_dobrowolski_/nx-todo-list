import { webAuthDomain } from './web-auth-domain';

describe('webAuthDomain', () => {
  it('should work', () => {
    expect(webAuthDomain()).toEqual('web-auth-domain');
  });
});

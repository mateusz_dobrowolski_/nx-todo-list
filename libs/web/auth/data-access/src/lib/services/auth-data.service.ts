import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginPayload } from '../../../../domain/src/lib/payloads/login.payload';
import { Observable } from 'rxjs';
import { SignUpPayload } from '../../../../domain/src/lib/payloads/sign-up.payload';
import { LoginResponse } from '../../../../../../shared/api-models/src/lib/responses/login.response';

@Injectable({
  providedIn: 'root',
})
export class AuthDataService {
  readonly endpoints = {
    login: () => `api/auth/login`,
    signUp: () => 'api/auth/sign-up',
  };

  constructor(private readonly httpClient: HttpClient) {}

  login(loginPayload: LoginPayload): Observable<LoginResponse> {
    return this.httpClient.post<LoginResponse>(
      this.endpoints.login(),
      loginPayload,
    );
  }

  signUp(signUpPayload: SignUpPayload): Observable<void> {
    return this.httpClient.post<void>(this.endpoints.signUp(), signUpPayload);
  }
}

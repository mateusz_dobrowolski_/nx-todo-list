import { initialState, AUTH_FEATURE_KEY, AuthState } from './auth.reducer';
import { authQuery } from './auth.selectors';

describe('Auth Selectors', () => {
  let storeState: { [AUTH_FEATURE_KEY]: AuthState };

  beforeEach(() => {
    storeState = {
      [AUTH_FEATURE_KEY]: initialState,
    };
  });
});

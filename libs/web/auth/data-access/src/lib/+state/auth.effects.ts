import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';
import { fromAuthActions } from './auth.actions';
import { AuthPartialState } from './auth.reducer';
import { AuthDataService } from '../services/auth-data.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { LoginResponse } from '../../../../../../shared/api-models/src/lib/responses/login.response';

@Injectable()
export class AuthEffects {
  @Effect()
  login$ = this.dp.fetch(fromAuthActions.Types.Login, {
    run: (action: fromAuthActions.Login) =>
      this.authDataService
        .login(action.payload)
        .pipe(
          map((res: LoginResponse) => new fromAuthActions.LoginSuccess(res)),
        ),
    onError: (action: fromAuthActions.Login, error: HttpErrorResponse) =>
      new fromAuthActions.LoginFail(error),
  });

  @Effect()
  signUp$ = this.dp.fetch(fromAuthActions.Types.SignUp, {
    run: (action: fromAuthActions.SignUp) =>
      this.authDataService
        .signUp(action.payload)
        .pipe(map(() => new fromAuthActions.SignUpSuccess())),
    onError: (action: fromAuthActions.SignUp, error: HttpErrorResponse) =>
      new fromAuthActions.SignUpFail(error),
  });

  constructor(
    private dp: DataPersistence<AuthPartialState>,
    private actions$: Actions,
    private router: Router,
    private authDataService: AuthDataService,
  ) {}
}

import { fromAuthActions } from './auth.actions';

export const AUTH_FEATURE_KEY = 'auth';

export interface AuthState {
  loggingIn: boolean;
}

export interface AuthPartialState {
  readonly [AUTH_FEATURE_KEY]: AuthState;
}

export const initialState: AuthState = {
  loggingIn: false,
};

export function authReducer(
  state: AuthState = initialState,
  action: fromAuthActions.CollectiveType,
): AuthState {
  switch (action.type) {
    case fromAuthActions.Types.Login:
      state = {
        ...state,
        loggingIn: true,
      };
      break;

    case fromAuthActions.Types.LoginSuccess:
      state = {
        ...state,
        loggingIn: false,
      };
      break;

    case fromAuthActions.Types.LoginFail:
      state = {
        ...state,
        loggingIn: false,
      };
      break;
  }

  return state;
}

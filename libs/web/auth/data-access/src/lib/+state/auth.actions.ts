import { Action } from '@ngrx/store';
import { LoginPayload } from '../../../../domain/src/lib/payloads/login.payload';
import { SignUpPayload } from '../../../../domain/src/lib/payloads/sign-up.payload';
import { LoginResponse } from '../../../../../../shared/api-models/src/lib/responses/login.response';

export namespace fromAuthActions {
  export enum Types {
    Login = '[Auth] Login',
    LoginSuccess = '[Auth] Login Success',
    LoginFail = '[Auth] Login Fail',
    SignUp = '[Auth] SignUp',
    SignUpSuccess = '[Auth] SignUp Success',
    SignUpFail = '[Auth] SignUp Fail',
  }

  export class Login implements Action {
    readonly type = Types.Login;

    constructor(public readonly payload: LoginPayload) {}
  }

  export class LoginSuccess implements Action {
    readonly type = Types.LoginSuccess;

    constructor(public readonly payload: LoginResponse) {}
  }

  export class LoginFail implements Action {
    readonly type = Types.LoginFail;

    constructor(public readonly payload: unknown) {}
  }

  export class SignUp implements Action {
    readonly type = Types.SignUp;

    constructor(public readonly payload: SignUpPayload) {}
  }

  export class SignUpSuccess implements Action {
    readonly type = Types.SignUpSuccess;
  }

  export class SignUpFail implements Action {
    readonly type = Types.SignUpFail;

    constructor(public readonly payload: unknown) {}
  }

  export type CollectiveType = Action;
}

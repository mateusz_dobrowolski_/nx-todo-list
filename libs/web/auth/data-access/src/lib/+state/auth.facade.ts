import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthPartialState } from './auth.reducer';
import { Observable } from 'rxjs';
import { authQuery } from './auth.selectors';
import { SignUpPayload } from '../../../../domain/src/lib/payloads/sign-up.payload';
import { LoginPayload } from '../../../../domain/src/lib/payloads/login.payload';
import { fromAuthActions } from './auth.actions';
import { ActionStatusResolverService } from '../../../../../shared/util-ngrx/src';
import { LoginResponse } from '../../../../../../shared/api-models/src/lib/responses/login.response';

@Injectable()
export class AuthFacade {
  loggingIn$: Observable<boolean>;

  constructor(
    private store: Store<AuthPartialState>,
    private readonly actionStatusResolverService: ActionStatusResolverService,
  ) {
    this.initDataStreams();
  }

  login(payload: LoginPayload): Observable<LoginResponse> {
    return this.actionStatusResolverService.resolve(
      new fromAuthActions.Login(payload),
      fromAuthActions.Types.LoginSuccess,
      fromAuthActions.Types.LoginFail,
    );
  }

  signUp(payload: SignUpPayload): Observable<void> {
    return this.actionStatusResolverService.resolve(
      new fromAuthActions.SignUp(payload),
      fromAuthActions.Types.SignUpSuccess,
      fromAuthActions.Types.SignUpFail,
    );
  }

  private initDataStreams(): void {
    this.loggingIn$ = this.store.select(authQuery.selectLoggingIn);
  }
}

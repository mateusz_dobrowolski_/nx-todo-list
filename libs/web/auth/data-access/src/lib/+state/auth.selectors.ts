import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AUTH_FEATURE_KEY, AuthState } from './auth.reducer';

// Lookup the 'Auth' feature state managed by NgRx
const selectAuthState = createFeatureSelector<AuthState>(AUTH_FEATURE_KEY);

const selectLoggingIn = createSelector(
  selectAuthState,
  state => state.loggingIn,
);

export const authQuery = {
  selectLoggingIn,
};

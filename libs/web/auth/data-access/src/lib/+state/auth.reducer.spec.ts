import { fromAuthActions } from './auth.actions';
import { AuthState, initialState, authReducer } from './auth.reducer';

describe('Auth Reducer', () => {
  let state: AuthState;

  beforeEach(() => {
    state = { ...initialState };
  });

  describe('unknown action', () => {
    test('returns the initial state', () => {
      const action = {} as any;
      const result = authReducer(state, action);

      expect(result).toBe(state);
    });
  });
});

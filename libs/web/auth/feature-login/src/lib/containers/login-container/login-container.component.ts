import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AuthFacade } from '@mateusz-d-todo-list/web/auth/data-access';
import { LoginPayload } from '../../../../../domain/src/lib/payloads/login.payload';
import { LoginResponse } from '../../../../../../../shared/api-models/src/lib/responses/login.response';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'mateusz-d-todo-list-login-container',
  templateUrl: './login-container.component.html',
  styleUrls: ['./login-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginContainerComponent {
  constructor(
    private readonly authFacade: AuthFacade,
    private readonly snackBar: MatSnackBar,
  ) {}

  onLogin(payload: LoginPayload): void {
    this.authFacade.login(payload).subscribe({
      next: (loginResponse: LoginResponse) =>
        this.loginSuccessHandler(loginResponse),
      error: (error: unknown) => this.loginFailHandler(error),
    });
  }

  private loginSuccessHandler(response: LoginResponse): void {
    this.snackBar.open('Login success');
  }

  private loginFailHandler(error: unknown): void {
    this.snackBar.open('Login fail');
  }
}

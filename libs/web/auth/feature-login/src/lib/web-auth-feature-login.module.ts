import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebAuthFeatureLoginRoutingModule } from './web-auth-feature-login-routing.module';
import { LoginContainerComponent } from './containers/login-container/login-container.component';
import { WebAuthUiLoginFormModule } from '@mateusz-d-todo-list/web/auth/ui-login-form';
import { WebAuthDataAccessModule } from '@mateusz-d-todo-list/web/auth/data-access';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
  imports: [
    CommonModule,
    WebAuthFeatureLoginRoutingModule,
    WebAuthUiLoginFormModule,
    WebAuthDataAccessModule,
    MatSnackBarModule,
  ],
  declarations: [LoginContainerComponent],
})
export class WebAuthFeatureLoginModule {}

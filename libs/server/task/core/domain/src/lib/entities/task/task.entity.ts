import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { isDefined } from '@mateusz-d-todo-list/shared/util-typescript';
import { UserEntity } from '@mateusz-d-todo-list/server/user/core/domain';
import { AbstractEntity } from '@mateusz-d-todo-list/server/shared/domain';

const userJoinColumnName = 'userId';

@Entity('task')
export class TaskEntity extends AbstractEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar' })
  name: string;

  @Column({ type: 'boolean' })
  done: boolean;

  @ManyToOne(() => UserEntity)
  @JoinColumn({ name: userJoinColumnName })
  user?: UserEntity;

  @Column({ type: 'int' })
  [userJoinColumnName]: number;

  update(data: { name?: string; done?: boolean }): void {
    if (isDefined(data.done)) {
      this.done = data.done;
    }

    if (isDefined(data.name)) {
      this.name = data.name;
    }
  }
}

import { TaskEntity } from '@mateusz-d-todo-list/server/task/core/domain';

export class TaskEntityFactory {
  static createEntity(userId: number, taskName: string): TaskEntity {
    const newEntity = new TaskEntity();
    newEntity.name = taskName;
    newEntity.done = false;
    newEntity.userId = userId;
    return newEntity;
  }
}

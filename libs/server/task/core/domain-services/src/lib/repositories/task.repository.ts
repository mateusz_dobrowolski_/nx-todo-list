import { TaskEntity } from '@mateusz-d-todo-list/server/task/core/domain';

export abstract class TaskRepository {
  abstract findAllByUserId(userId: number): Promise<TaskEntity[]>;

  abstract getById(id: number): Promise<TaskEntity | undefined>;

  abstract save(entity: TaskEntity): Promise<TaskEntity>;

  abstract delete(entity: TaskEntity): Promise<TaskEntity>;

  abstract findAll(): Promise<TaskEntity[]>;
}

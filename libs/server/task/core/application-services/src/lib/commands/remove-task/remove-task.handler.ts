import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { RemoveTaskCommand } from './remove-task.command';
import { TaskRepository } from '@mateusz-d-todo-list/server/task/core/domain-services';
import { TaskReadModel, taskReadModelFactory } from '../../read-models';
import {
  TaskAccessForbiddenException,
  TaskNotFoundException,
} from '@mateusz-d-todo-list/server/task/domain';
import { UserRole } from '@mateusz-d-todo-list/server/user/core/domain';

@CommandHandler(RemoveTaskCommand)
export class RemoveTaskHandler implements ICommandHandler<RemoveTaskCommand> {
  constructor(private taskRepository: TaskRepository) {}

  async execute(command: RemoveTaskCommand): Promise<TaskReadModel> {
    const task = await this.taskRepository.getById(command.taskId);
    if (!task) {
      throw new TaskNotFoundException();
    }

    if (
      task.userId !== command.authorizedUserData.id &&
      command.authorizedUserData.role !== UserRole.Admin
    ) {
      throw new TaskAccessForbiddenException();
    }

    const removedTask = await this.taskRepository.delete(task);

    return taskReadModelFactory(removedTask);
  }
}

import { AuthorizedUserDataModel } from '@mateusz-d-todo-list/server/auth/core/domain';

export class RemoveTaskCommand {
  constructor(
    public readonly taskId: number,
    public readonly authorizedUserData: AuthorizedUserDataModel,
  ) {}
}

import { AuthorizedUserDataModel } from '@mateusz-d-todo-list/server/auth/core/domain';

export class EditTaskCommand {
  constructor(
    public readonly taskId: number,
    public readonly data: { name?: string; done?: boolean },
    public readonly authorizedUserData: AuthorizedUserDataModel,
  ) {}
}

export class AddTaskCommand {
  constructor(
    public readonly userId: number,
    public readonly task: { name: string },
  ) {}
}

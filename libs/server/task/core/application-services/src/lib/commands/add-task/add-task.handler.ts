import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AddTaskCommand } from './add-task.command';
import { TaskRepository } from '@mateusz-d-todo-list/server/task/core/domain-services';
import { TaskReadModel, taskReadModelFactory } from '../../read-models';
import { TaskEntityFactory } from '@mateusz-d-todo-list/server/task/core/domain';

@CommandHandler(AddTaskCommand)
export class AddTaskHandler implements ICommandHandler<AddTaskCommand> {
  constructor(private taskRepository: TaskRepository) {}

  async execute(command: AddTaskCommand): Promise<TaskReadModel> {
    const { name } = command.task;
    const taskEntity = TaskEntityFactory.createEntity(command.userId, name);
    const savedTask = await this.taskRepository.save(taskEntity);
    return taskReadModelFactory(savedTask);
  }
}

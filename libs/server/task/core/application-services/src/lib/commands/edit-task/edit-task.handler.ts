import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { EditTaskCommand } from './edit-task.command';
import { TaskRepository } from '@mateusz-d-todo-list/server/task/core/domain-services';
import { TaskReadModel, taskReadModelFactory } from '../../read-models';
import {
  TaskAccessForbiddenException,
  TaskNotFoundException,
} from '@mateusz-d-todo-list/server/task/domain';
import { UserRole } from '@mateusz-d-todo-list/server/user/core/domain';

@CommandHandler(EditTaskCommand)
export class EditTaskHandler implements ICommandHandler<EditTaskCommand> {
  constructor(private taskRepository: TaskRepository) {}

  async execute(command: EditTaskCommand): Promise<TaskReadModel> {
    const taskEntity = await this.taskRepository.getById(command.taskId);
    if (!taskEntity) {
      throw new TaskNotFoundException();
    }
    if (
      taskEntity.userId !== command.authorizedUserData.id &&
      command.authorizedUserData.role !== UserRole.Admin
    ) {
      throw new TaskAccessForbiddenException();
    }
    taskEntity.update(command.data);
    const savedTaskEntity = await this.taskRepository.save(taskEntity);

    return taskReadModelFactory(savedTaskEntity);
  }
}

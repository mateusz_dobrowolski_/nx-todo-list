import { CqrsModule } from '@nestjs/cqrs';
import { DynamicModule, Module } from '@nestjs/common';
import { TaskFacade } from './task.facade';
import { AddTaskHandler, EditTaskHandler, RemoveTaskHandler } from './commands';
import { GetTasksHandler } from './queries';
import { ModuleInfrastructure } from '@mateusz-d-todo-list/server/shared/domain';

const QueryHandlers = [
  AddTaskHandler,
  RemoveTaskHandler,
  EditTaskHandler,
  GetTasksHandler,
];

@Module({})
export class ServerTaskCoreApplicationServicesModule {
  static withInfrastructure(
    infrastructure: ModuleInfrastructure,
  ): DynamicModule {
    return {
      module: ServerTaskCoreApplicationServicesModule,
      imports: [CqrsModule, ...infrastructure],
      providers: [TaskFacade, ...QueryHandlers],
      exports: [TaskFacade],
    };
  }
}

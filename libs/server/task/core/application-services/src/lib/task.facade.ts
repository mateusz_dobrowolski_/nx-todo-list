import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { AddTaskCommand, EditTaskCommand, RemoveTaskCommand } from './commands';
import { TaskListReadModel, TaskReadModel } from './read-models';
import { GetTasksQuery } from './queries';

@Injectable()
export class TaskFacade {
  constructor(private commandBus: CommandBus, private queryBus: QueryBus) {}

  addTask(command: AddTaskCommand): Promise<TaskReadModel> {
    return this.commandBus.execute<AddTaskCommand>(command);
  }

  editTask(command: EditTaskCommand): Promise<TaskReadModel> {
    return this.commandBus.execute(command);
  }

  removeTask(command: RemoveTaskCommand): Promise<TaskReadModel> {
    return this.commandBus.execute(command);
  }

  getTasks(query: GetTasksQuery): Promise<TaskListReadModel> {
    return this.queryBus.execute(query);
  }
}

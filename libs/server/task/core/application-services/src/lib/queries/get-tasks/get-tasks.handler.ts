import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';

import { GetTasksQuery } from './get-tasks.query';
import { TaskRepository } from '@mateusz-d-todo-list/server/task/core/domain-services';
import { TaskListReadModel, taskListReadModelFactory } from '../../read-models';
import { UserRole } from '@mateusz-d-todo-list/server/user/core/domain';

@QueryHandler(GetTasksQuery)
export class GetTasksHandler implements IQueryHandler<GetTasksQuery> {
  constructor(private readonly taskRepository: TaskRepository) {}

  async execute(query: GetTasksQuery): Promise<TaskListReadModel> {
    const taskEntities = await (query.authorizedUserData.role === UserRole.Admin
      ? this.taskRepository.findAll()
      : this.taskRepository.findAllByUserId(query.authorizedUserData.id));

    return taskListReadModelFactory(taskEntities);
  }
}

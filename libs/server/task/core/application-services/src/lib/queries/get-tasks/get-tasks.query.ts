import { IQuery } from '@nestjs/cqrs';
import { AuthorizedUserDataModel } from '@mateusz-d-todo-list/server/auth/core/domain';

export class GetTasksQuery implements IQuery {
  constructor(public readonly authorizedUserData: AuthorizedUserDataModel) {}
}

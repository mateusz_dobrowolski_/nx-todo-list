import { TaskReadModel } from '../task.read-model';
import { TaskEntity } from '@mateusz-d-todo-list/server/task/core/domain';

export const taskReadModelFactory = ({
  id,
  name,
  done,
  userId,
}: TaskEntity): TaskReadModel => {
  return new TaskReadModel({ id, name, done, userId });
};

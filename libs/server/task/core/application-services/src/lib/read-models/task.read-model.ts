import { ApiProperty } from '@nestjs/swagger';

export class TaskReadModel {
  @ApiProperty()
  id: number;

  @ApiProperty()
  name: string;

  @ApiProperty()
  done: boolean;

  @ApiProperty({ description: 'Task owner' })
  userId: number;

  constructor(data: {
    id: number;
    name: string;
    done: boolean;
    userId: number;
  }) {
    this.id = data.id;
    this.name = data.name;
    this.done = data.done;
    this.userId = data.userId;
  }
}

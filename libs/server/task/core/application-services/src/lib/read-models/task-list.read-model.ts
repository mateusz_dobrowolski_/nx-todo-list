import { AbstractListModel } from '@mateusz-d-todo-list/server/shared/domain';
import { TaskReadModel } from './task.read-model';
import { ApiProperty } from '@nestjs/swagger';

export class TaskListReadModel extends AbstractListModel<TaskReadModel> {
  @ApiProperty({
    isArray: true,
    type: TaskReadModel,
  })
  items: TaskReadModel[];

  constructor(items: TaskReadModel[]) {
    super();
    this.items = items;
  }
}

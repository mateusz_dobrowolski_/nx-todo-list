import { TaskEntity } from '@mateusz-d-todo-list/server/task/core/domain';
import {
  TaskListReadModel,
  taskReadModelFactory,
} from '@mateusz-d-todo-list/server/task/core/application-services';

export const taskListReadModelFactory = (
  taskEntities: TaskEntity[],
): TaskListReadModel => {
  const taskReadModels = taskEntities.map(entity =>
    taskReadModelFactory(entity),
  );
  return new TaskListReadModel(taskReadModels);
};

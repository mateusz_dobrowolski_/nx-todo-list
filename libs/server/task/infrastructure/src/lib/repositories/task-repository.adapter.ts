import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TaskRepository } from '@mateusz-d-todo-list/server/task/core/domain-services';
import { TaskEntity } from '@mateusz-d-todo-list/server/task/core/domain';

export class TaskRepositoryAdapter implements TaskRepository {
  constructor(
    @InjectRepository(TaskEntity)
    private typeOrmRepository: Repository<TaskEntity>,
  ) {}

  async delete(entity: TaskEntity): Promise<TaskEntity> {
    return this.typeOrmRepository.remove(entity);
  }

  async findAllByUserId(userId: number): Promise<TaskEntity[]> {
    return this.typeOrmRepository.find({
      where: {
        userId,
      },
    });
  }

  async getById(id: number): Promise<TaskEntity | undefined> {
    return this.typeOrmRepository.findOne(id);
  }

  async save(entity: TaskEntity): Promise<TaskEntity> {
    return this.typeOrmRepository.save(entity);
  }

  async findAll(): Promise<TaskEntity[]> {
    return this.typeOrmRepository.find();
  }
}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TaskRepositoryAdapter } from './repositories/task-repository.adapter';
import { TaskEntity } from '@mateusz-d-todo-list/server/task/core/domain';
import { TaskRepository } from '@mateusz-d-todo-list/server/task/core/domain-services';

const typeormModule = TypeOrmModule.forFeature([TaskEntity]);

@Module({
  imports: [typeormModule],
  providers: [
    {
      provide: TaskRepository,
      useClass: TaskRepositoryAdapter,
    },
  ],
  exports: [TaskRepository],
})
export class ServerTaskInfrastructureModule {}

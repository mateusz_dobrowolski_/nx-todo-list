import { Module } from '@nestjs/common';
import { TaskController } from './controllers/task.controller';
import { ServerTaskShellModule } from '@mateusz-d-todo-list/server/task/shell';

@Module({
  imports: [ServerTaskShellModule],
  controllers: [TaskController],
})
export class ServerTaskApiRestModule {}

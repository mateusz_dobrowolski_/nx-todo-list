import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class AddTaskDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  name: string;
}

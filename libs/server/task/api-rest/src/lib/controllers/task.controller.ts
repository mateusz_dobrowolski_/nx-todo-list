import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
} from '@nestjs/common';
import { AddTaskDto } from '../dtos/add-task.dto';
import { EditTaskDto } from '../dtos/edit-task.dto';
import {
  AddTaskCommand,
  EditTaskCommand,
  GetTasksQuery,
  RemoveTaskCommand,
  TaskFacade,
  TaskListReadModel,
  TaskReadModel,
} from '@mateusz-d-todo-list/server/task/core/application-services';
import {
  Auth,
  AuthorizedUserData,
  AuthorizedUserDataModel,
} from '@mateusz-d-todo-list/server/auth/public';
import { ApiCreatedResponse, ApiOkResponse } from '@nestjs/swagger';
import { AllowedRoles } from '@mateusz-d-todo-list/server/auth/infrastructure';
import { UserRole } from '@mateusz-d-todo-list/server/user/core/domain';

@AllowedRoles(UserRole.User, UserRole.Admin)
@Auth()
@Controller('tasks')
export class TaskController {
  constructor(private taskFacade: TaskFacade) {}

  @Get()
  @ApiOkResponse({
    description: 'List of authorized user tasks.',
    type: TaskListReadModel,
  })
  getList(
    @AuthorizedUserData() authorizedUserData: AuthorizedUserDataModel,
  ): Promise<TaskListReadModel> {
    return this.taskFacade.getTasks(new GetTasksQuery(authorizedUserData));
  }

  @Post()
  @ApiCreatedResponse({
    description: 'Task created.',
    type: TaskReadModel,
  })
  addTask(
    @Body() dto: AddTaskDto,
    @AuthorizedUserData() authorizedUserData: AuthorizedUserDataModel,
  ): Promise<TaskReadModel> {
    return this.taskFacade.addTask(
      new AddTaskCommand(authorizedUserData.id, {
        name: dto.name,
      }),
    );
  }

  @Put(':taskId')
  @ApiOkResponse({
    description: 'Task modified.',
    type: TaskReadModel,
  })
  editTask(
    @Param('taskId', ParseIntPipe) taskId: number,
    @Body() dto: EditTaskDto,
    @AuthorizedUserData() authorizedUserData: AuthorizedUserDataModel,
  ): Promise<TaskReadModel> {
    return this.taskFacade.editTask(
      new EditTaskCommand(
        taskId,
        {
          done: dto.done,
          name: dto.name,
        },
        authorizedUserData,
      ),
    );
  }

  @Delete(':taskId')
  @ApiOkResponse({
    description: 'Task removed.',
    type: TaskReadModel,
  })
  deleteTask(
    @Param('taskId', ParseIntPipe) taskId: number,
    @AuthorizedUserData() authorizedUserData: AuthorizedUserDataModel,
  ): Promise<TaskReadModel> {
    return this.taskFacade.removeTask(
      new RemoveTaskCommand(taskId, authorizedUserData),
    );
  }
}

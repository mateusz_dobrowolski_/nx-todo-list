import { Module } from '@nestjs/common';
import { ServerTaskInfrastructureModule } from '@mateusz-d-todo-list/server/task/infrastructure';
import { ServerTaskCoreApplicationServicesModule } from '@mateusz-d-todo-list/server/task/core/application-services';

@Module({
  imports: [
    ServerTaskCoreApplicationServicesModule.withInfrastructure([
      ServerTaskInfrastructureModule,
    ]),
  ],
  exports: [ServerTaskCoreApplicationServicesModule],
})
export class ServerTaskShellModule {}

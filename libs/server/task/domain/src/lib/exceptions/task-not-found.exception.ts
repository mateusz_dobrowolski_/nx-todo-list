import { CoreNotFoundException } from '@mateusz-d-todo-list/server/shared/domain';

export class TaskNotFoundException extends CoreNotFoundException {}

import { CoreForbiddenException } from '@mateusz-d-todo-list/server/shared/domain';

export class TaskAccessForbiddenException extends CoreForbiddenException {}

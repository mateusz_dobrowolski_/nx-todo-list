import { serverUserCore } from './server-user-core';

describe('serverUserCore', () => {
  it('should work', () => {
    expect(serverUserCore()).toEqual('server-user-core');
  });
});

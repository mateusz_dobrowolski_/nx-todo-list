import { userReadModelFactory } from './user-read-model.factory';
import { UserListReadModel } from '../user-list.read-model';

export const userListReadModelFactory = (
  data: { id: number; email: string }[],
) => {
  const readModels = data.map(elem => userReadModelFactory(elem));
  return new UserListReadModel(readModels);
};

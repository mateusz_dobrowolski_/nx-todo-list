import { ApiProperty } from '@nestjs/swagger';
import { UserReadModel } from './user.read-model';
import { AbstractListModel } from '@mateusz-d-todo-list/server/shared/domain';

export class UserListReadModel extends AbstractListModel<UserReadModel> {
  @ApiProperty({
    isArray: true,
    type: UserReadModel,
  })
  items: UserReadModel[];

  constructor(items: UserReadModel[]) {
    super();
    this.items = items;
  }
}

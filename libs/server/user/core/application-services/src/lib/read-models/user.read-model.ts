import { ApiProperty } from '@nestjs/swagger';

export class UserReadModel {
  @ApiProperty()
  id: number;

  @ApiProperty()
  email: string;

  constructor(id: number, email: string) {
    this.id = id;
    this.email = email;
  }
}

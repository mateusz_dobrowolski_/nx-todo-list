import { UserReadModel } from '../user.read-model';

export const userReadModelFactory = (data: { id: number; email: string }) =>
  new UserReadModel(data.id, data.email);

import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetUserListQuery } from './get-user-list.query';
import { UserRepository } from '@mateusz-d-todo-list/server/user/core/domain-services';
import { UserListReadModel, userListReadModelFactory } from '../../read-models';

@QueryHandler(GetUserListQuery)
export class GetUserListHandler
  implements IQueryHandler<GetUserListQuery, UserListReadModel> {
  constructor(private userRepository: UserRepository) {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async execute(query: GetUserListQuery): Promise<UserListReadModel> {
    const userEntities = await this.userRepository.findAll();
    return userListReadModelFactory(userEntities);
  }
}

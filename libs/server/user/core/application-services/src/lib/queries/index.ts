import { GetUserListHandler } from './get-user-list/get-user-list.handler';

export const QUERY_HANDLERS = [GetUserListHandler];

export * from './get-user-list/get-user-list.handler';
export * from './get-user-list/get-user-list.query';

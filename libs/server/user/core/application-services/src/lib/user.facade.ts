import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { UserListReadModel } from './read-models';
import { GetUserListQuery } from './queries/get-user-list/get-user-list.query';

@Injectable()
export class UserFacade {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  getAllUsers(query: GetUserListQuery): Promise<UserListReadModel> {
    return this.queryBus.execute<GetUserListQuery, UserListReadModel>(query);
  }
}

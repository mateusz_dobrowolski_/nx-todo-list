import { DynamicModule, Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { COMMAND_HANDLERS } from './commands';
import { QUERY_HANDLERS } from './queries';
import { UserFacade } from './user.facade';
import { ModuleInfrastructure } from '@mateusz-d-todo-list/server/shared/domain';

@Module({})
export class ServerUserApplicationServicesModule {
  static withInfrastructure(
    infrastructure: ModuleInfrastructure,
  ): DynamicModule {
    return {
      module: ServerUserApplicationServicesModule,
      imports: [CqrsModule, ...infrastructure],
      providers: [...COMMAND_HANDLERS, ...QUERY_HANDLERS, UserFacade],
      exports: [UserFacade],
    };
  }
}

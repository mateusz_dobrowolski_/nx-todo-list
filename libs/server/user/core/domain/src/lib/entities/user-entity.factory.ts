import {
  UserEntity,
  UserRole,
} from '@mateusz-d-todo-list/server/user/core/domain';

export class UserEntityFactory {
  static createNewUser(
    email: string,
    encodedPassword: string,
    role: UserRole,
  ): UserEntity {
    const newUserEntity: UserEntity = new UserEntity();
    newUserEntity.email = email;
    newUserEntity.password = encodedPassword;
    newUserEntity.role = role;
    return newUserEntity;
  }
}

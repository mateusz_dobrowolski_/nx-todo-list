import { UserEntity } from '@mateusz-d-todo-list/server/user/core/domain';

export abstract class UserRepository {
  abstract findAll(): Promise<UserEntity[]>;

  abstract getById(id: number): Promise<UserEntity | undefined>;

  abstract getByEmail(email: string): Promise<UserEntity | undefined>;

  abstract save(entity: UserEntity): Promise<UserEntity>;

  abstract delete(entity: UserEntity): Promise<UserEntity>;

  abstract createNewUser(newUserEntity: UserEntity): Promise<UserEntity>;
}

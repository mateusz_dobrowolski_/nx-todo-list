import { Controller, Get } from '@nestjs/common';
import { Auth } from '@mateusz-d-todo-list/server/auth/public';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import {
  GetUserListQuery,
  UserFacade,
  UserListReadModel,
} from '@mateusz-d-todo-list/server/user/core/application-services';
import { AllowedRoles } from '@mateusz-d-todo-list/server/auth/infrastructure';
import { UserRole } from '@mateusz-d-todo-list/server/user/core/domain';

@AllowedRoles(UserRole.Admin)
@Auth()
@ApiTags('users')
@Controller('users')
export class UserController {
  constructor(private userFacade: UserFacade) {}

  @Get()
  @ApiOkResponse({
    description: 'List of all users.',
    type: UserListReadModel,
  })
  async getList(): Promise<UserListReadModel> {
    return this.userFacade.getAllUsers(new GetUserListQuery());
  }
}

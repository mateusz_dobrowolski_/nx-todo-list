import { Module } from '@nestjs/common';
import { UserController } from './controllers/user.controller';
import { ServerUserShellModule } from '@mateusz-d-todo-list/server/user/shell';

@Module({
  controllers: [UserController],
  imports: [ServerUserShellModule],
})
export class ServerUserApiRestModule {}

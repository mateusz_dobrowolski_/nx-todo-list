import { Module } from '@nestjs/common';
import { ServerUserApplicationServicesModule } from '@mateusz-d-todo-list/server/user/core/application-services';
import { ServerUserInfrastructureModule } from '@mateusz-d-todo-list/server/user/infrastructure';

@Module({
  imports: [
    ServerUserApplicationServicesModule.withInfrastructure([
      ServerUserInfrastructureModule,
    ]),
  ],
  exports: [ServerUserApplicationServicesModule],
})
export class ServerUserShellModule {}

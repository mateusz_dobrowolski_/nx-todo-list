import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserRepository } from '@mateusz-d-todo-list/server/user/core/domain-services';
import { UserEntity } from '@mateusz-d-todo-list/server/user/core/domain';

export class UserRepositoryAdapter implements UserRepository {
  constructor(
    @InjectRepository(UserEntity)
    private typeOrmRepository: Repository<UserEntity>,
  ) {}

  async delete(entity: UserEntity): Promise<UserEntity> {
    return this.typeOrmRepository.remove(entity);
  }

  async findAll(): Promise<UserEntity[]> {
    return this.typeOrmRepository.find();
  }

  async getById(id: number): Promise<UserEntity | undefined> {
    return this.typeOrmRepository.findOne(id);
  }

  async getByEmail(email: string): Promise<UserEntity | undefined> {
    return this.typeOrmRepository.findOne({ where: { email } });
  }

  async save(entity: UserEntity): Promise<UserEntity> {
    return this.typeOrmRepository.save(entity);
  }

  async createNewUser(newUserEntity: UserEntity): Promise<UserEntity> {
    return this.typeOrmRepository.save(newUserEntity);
  }
}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from '@mateusz-d-todo-list/server/user/core/domain';
import { UserRepository } from '@mateusz-d-todo-list/server/user/core/domain-services';
import { UserRepositoryAdapter } from './adapters';

const typeormModule = TypeOrmModule.forFeature([UserEntity]);

@Module({
  imports: [typeormModule],
  providers: [
    {
      provide: UserRepository,
      useClass: UserRepositoryAdapter,
    },
  ],
  exports: [UserRepository],
})
export class ServerUserInfrastructureModule {}

import { Global, Module } from '@nestjs/common';
import { ConfigModule as NestConfigModule } from '@nestjs/config';
import {
  todoApiConfig,
  TodoListApiConfigService,
} from './todo-list-api-config.service.ts';

@Global()
@Module({
  imports: [
    NestConfigModule.forRoot({
      load: [todoApiConfig],
      isGlobal: true,
    }),
  ],
  exports: [TodoListApiConfigService],
  providers: [TodoListApiConfigService],
})
export class ConfigModule {}

import { Injectable } from '@nestjs/common';
import { ConfigService, registerAs } from '@nestjs/config';

const CONFIG_NAMESPACE = 'todo-list-api-service';

export const todoApiConfig = registerAs(CONFIG_NAMESPACE, () => {
  return {
    auth: {
      jwtSecret: process.env.AUTH_JWT_SECRET,
      jwtExpirationTimeInHours: 2,
      refreshTokenExpirationTimeInHours: 48,
    },
    db: {
      host: process.env.TYPEORM_HOST,
      username: process.env.TYPEORM_USERNAME,
      password: process.env.TYPEORM_PASSWORD,
      database: process.env.TYPEORM_DATABASE,
      port: +process.env.TYPEORM_PORT!,
    },
  };
});

@Injectable()
export class TodoListApiConfigService {
  constructor(private configService: ConfigService) {}

  getJwtSecret(): string {
    return this.configService.get(CONFIG_NAMESPACE + '.auth.jwtSecret')!;
  }

  getJwtExpirationTimeInHours(): number {
    return this.configService.get(
      CONFIG_NAMESPACE + '.auth.jwtExpirationTimeInHours',
    )!;
  }

  getJwtRefreshExpirationTimeInHours(): number {
    return this.configService.get(
      CONFIG_NAMESPACE + '.auth.refreshTokenExpirationTimeInHours',
    )!;
  }

  getDbConfig(): {
    host: string;
    database: string;
    password: string;
    port: number;
    username: string;
  } {
    return this.configService.get(CONFIG_NAMESPACE + '.db')!;
  }
}

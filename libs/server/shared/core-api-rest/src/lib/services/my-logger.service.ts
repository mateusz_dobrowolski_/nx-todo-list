import { LoggerService } from '@nestjs/common';

// Implementation of that service can be extended by any custom logic, Dependency Injection and so on...
export class MyLoggerService implements LoggerService {
  error(message: unknown, trace?: string, context?: string): void {
    console.error(message, {
      context,
      trace,
    });
  }

  log(message: unknown, context?: string): void {
    console.log(message, {
      context,
    });
  }

  warn(message: unknown, context?: string): void {
    console.warn(message, {
      context,
    });
  }
}

import {
  BadRequestException,
  CallHandler,
  Catch,
  ConflictException,
  ExecutionContext,
  ForbiddenException,
  NestInterceptor,
  NotFoundException,
  UnauthorizedException,
  UnprocessableEntityException,
} from '@nestjs/common';
import {
  CoreBadRequestException,
  CoreForbiddenException,
  CoreInternalException,
  CoreNotFoundException,
  CoreResourceAlreadyExistsException,
  CoreUnauthorizedException,
  CoreUnprocessableEntityException,
} from '@mateusz-d-todo-list/server/shared/domain';
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Catch()
export class AllExceptionInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<unknown> {
    return next
      .handle()
      .pipe(catchError((error: unknown) => this.handleException(error)));
  }

  private handleException(exception: unknown): Observable<never> {
    if (
      exception instanceof CoreNotFoundException ||
      exception instanceof EntityNotFoundError
    ) {
      return throwError(new NotFoundException(exception.message));
    }
    if (exception instanceof CoreUnauthorizedException) {
      return throwError(new UnauthorizedException(exception.message));
    }
    if (exception instanceof CoreUnprocessableEntityException) {
      return throwError(new UnprocessableEntityException(exception.message));
    }
    if (exception instanceof CoreResourceAlreadyExistsException) {
      return throwError(new ConflictException(exception.message));
    }

    if (exception instanceof CoreBadRequestException) {
      return throwError(new BadRequestException(exception.message));
    }

    if (exception instanceof CoreForbiddenException) {
      return throwError(new ForbiddenException(exception.message));
    }

    return throwError(new CoreInternalException(exception));
  }
}

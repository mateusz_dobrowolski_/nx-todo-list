import {
  CallHandler,
  ExecutionContext,
  HttpException,
  Injectable,
  Logger,
  LoggerService,
  NestInterceptor,
} from '@nestjs/common';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  private readonly loggerService: LoggerService = new Logger(
    LoggingInterceptor.name, // Context for the logger
  );

  intercept(context: ExecutionContext, next: CallHandler): Observable<unknown> {
    const processingStartTimestamp = Date.now();
    return next
      .handle()
      .pipe(
        catchError((error: unknown) =>
          this.handleError(processingStartTimestamp, error),
        ),
      );
  }

  private handleError(
    processingStartTimestamp: number,
    error: unknown,
  ): Observable<never> {
    if (error instanceof HttpException && error.getStatus() < 500) {
      // expected errors
      return throwError(error);
    }

    return this.handleUnexpectedError(processingStartTimestamp, error);
  }

  private handleUnexpectedError(
    processingStartTimestamp: number,
    error: unknown,
  ): Observable<never> {
    // enrich error information by request processing time info
    const stackTrace: string | undefined =
      error instanceof Error ? error.stack : undefined;
    const processingEndDate = new Date();
    const processingDurationInMs =
      processingEndDate.getTime() - processingStartTimestamp;

    this.loggerService.error(
      {
        requestProcessingInfo: {
          processingStart: new Date(processingStartTimestamp),
          processingEnd: processingEndDate,
          'processingDuration[ms]': processingDurationInMs,
        },
        error,
      },
      stackTrace,
    );
    return throwError(error);
  }
}

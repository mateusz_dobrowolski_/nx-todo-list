import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  ConfigModule,
  TodoListApiConfigService,
} from '@mateusz-d-todo-list/server/shared/config';

const typeOrmModule = TypeOrmModule.forRootAsync({
  useFactory: async (configService: TodoListApiConfigService) => {
    const {
      host,
      database,
      password,
      port,
      username,
    } = configService.getDbConfig();
    return {
      type: 'postgres' as const,
      host,
      port,
      username,
      password,
      database,
      autoLoadEntities: true,
      synchronize: false,
      migrationsRun: false,
    };
  },
  inject: [TodoListApiConfigService],
});

@Module({
  imports: [typeOrmModule, ConfigModule],
  exports: [typeOrmModule],
})
export class TypeormRootModule {}

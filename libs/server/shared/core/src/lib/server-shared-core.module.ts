import { Global, HttpModule, Module } from '@nestjs/common';
import { TypeormRootModule } from './modules/typeorm-root.module';

@Global()
@Module({
  imports: [HttpModule, TypeormRootModule],
  exports: [HttpModule],
})
export class ServerSharedCoreModule {

}

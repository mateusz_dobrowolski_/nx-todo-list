import { ModuleMetadata, Type } from '@nestjs/common';

export type ModuleInfrastructure = ModuleMetadata['imports'] & Array<Type>;

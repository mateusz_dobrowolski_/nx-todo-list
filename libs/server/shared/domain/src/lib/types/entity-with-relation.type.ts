import { AbstractEntity } from '../models';

type RequireKeys<T, K extends keyof T> = Required<Pick<T, K>> & Omit<T, K>;

type RelationKeys<T> = {
  [P in keyof T]: T[P] extends AbstractEntity | undefined ? P : never;
}[keyof T];

export type EntityWithRelation<
  Entity extends AbstractEntity,
  Keys extends RelationKeys<Entity>
> = RequireKeys<Entity, Keys>;

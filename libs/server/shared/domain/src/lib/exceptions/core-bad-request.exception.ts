import { CoreException } from './core.exception';

export class CoreBadRequestException extends CoreException {}

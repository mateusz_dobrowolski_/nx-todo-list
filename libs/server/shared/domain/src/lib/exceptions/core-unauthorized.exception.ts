import { CoreException } from './core.exception';

export class CoreUnauthorizedException extends CoreException {}

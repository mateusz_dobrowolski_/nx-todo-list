import { CoreException } from './core.exception';

export class CoreForbiddenException extends CoreException {}

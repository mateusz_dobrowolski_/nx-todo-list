import { InternalServerErrorException } from '@nestjs/common';

export class CoreInternalException extends InternalServerErrorException {
  constructor(public readonly internalErrorData: unknown) {
    super();
  }
}

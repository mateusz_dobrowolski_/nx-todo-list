export abstract class AbstractListModel<T> {
  abstract readonly items: T[];
}

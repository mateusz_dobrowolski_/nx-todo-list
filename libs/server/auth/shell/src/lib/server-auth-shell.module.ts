import { Module } from '@nestjs/common';
import { ServerAuthInfrastructureModule } from '@mateusz-d-todo-list/server/auth/infrastructure';
import { ServerAuthCoreApplicationServicesModule } from '@mateusz-d-todo-list/server/auth/core/application-services';
import { ServerUserInfrastructureModule } from '@mateusz-d-todo-list/server/user/infrastructure';

@Module({
  imports: [
    ServerAuthCoreApplicationServicesModule.withInfrastructure([
      ServerAuthInfrastructureModule,
      ServerUserInfrastructureModule,
    ]),
  ],
  exports: [ServerAuthCoreApplicationServicesModule],
})
export class ServerAuthShellModule {}

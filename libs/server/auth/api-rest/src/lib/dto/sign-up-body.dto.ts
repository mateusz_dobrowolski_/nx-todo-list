import { IsDefined, IsEmail, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SignUpBodyDto {
  @IsDefined()
  @IsString()
  @IsEmail()
  @ApiProperty({ example: 'foo@bar.baz' })
  email: string;

  @IsDefined()
  @IsString()
  @ApiProperty({ example: '*)G&(@#GFG^F6g' })
  password: string;
}

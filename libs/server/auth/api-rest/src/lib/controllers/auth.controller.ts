import { Body, Controller, Param, Post } from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiTags,
} from '@nestjs/swagger';
import { LoginBodyDto } from '../dto/login-body.dto';
import {
  AccessTokenReadModel,
  AuthFacade,
} from '@mateusz-d-todo-list/server/auth/core/application-services';
import { SignUpBodyDto } from '../dto/sign-up-body.dto';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authFacade: AuthFacade) {}

  @Post('login')
  @ApiCreatedResponse({
    description: 'User successfully logged in.',
    type: AccessTokenReadModel,
  })
  async getAccessToken(
    @Body() loginBodyDto: LoginBodyDto,
  ): Promise<AccessTokenReadModel> {
    return this.authFacade.login(loginBodyDto.email, loginBodyDto.password);
  }

  @Post('sign-up')
  @ApiNoContentResponse({
    description: 'User successfully signed up.',
  })
  async signUp(@Body() signUpBodyDto: SignUpBodyDto): Promise<void> {
    return this.authFacade.signUp(signUpBodyDto.email, signUpBodyDto.password);
  }

  @Post('refresh-token/:refreshToken')
  @ApiCreatedResponse({
    description: 'Auth token successfully refreshed.',
    type: AccessTokenReadModel,
  })
  async refreshAccessToken(
    @Param('refreshToken') refreshToken: string,
  ): Promise<AccessTokenReadModel> {
    return this.authFacade.refreshToken(refreshToken);
  }
}

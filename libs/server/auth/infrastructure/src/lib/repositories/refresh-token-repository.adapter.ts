import { RefreshTokenRepository } from '@mateusz-d-todo-list/server/auth/core/domain-services';
import {
  RefreshTokenEntity,
  RefreshTokenEntityWithUser,
} from '@mateusz-d-todo-list/server/auth/core/domain';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';

@Injectable()
export class RefreshTokenRepositoryAdapter implements RefreshTokenRepository {
  constructor(
    @InjectRepository(RefreshTokenEntity)
    private typeOrmRepository: Repository<RefreshTokenEntity>,
    private readonly connection: Connection,
  ) {}

  async renew(entity: RefreshTokenEntity): Promise<RefreshTokenEntity> {
    const userId: number = entity.userId;
    return this.connection
      .createEntityManager()
      .transaction(async transactionalEntityManager => {
        await transactionalEntityManager
          .getRepository(RefreshTokenEntity)
          .delete({ userId });
        return transactionalEntityManager.save(entity);
      });
  }

  delete(entity: RefreshTokenEntity): Promise<RefreshTokenEntity> {
    return this.typeOrmRepository.remove(entity);
  }

  getByUuid(uuid: string): Promise<RefreshTokenEntityWithUser | undefined> {
    return this.typeOrmRepository.findOne({
      where: { uuid },
      relations: ['user'],
    }) as Promise<RefreshTokenEntityWithUser | undefined>;
  }
}

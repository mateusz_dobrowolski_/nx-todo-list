import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { AuthorizedUserDataModel } from '@mateusz-d-todo-list/server/auth/core/domain';

export const AuthorizedUserData = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    return request.user as AuthorizedUserDataModel;
  },
);

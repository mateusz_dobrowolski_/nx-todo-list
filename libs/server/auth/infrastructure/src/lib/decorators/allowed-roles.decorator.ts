import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { metadataUserRolesKey } from '../consts';
import { UserRoleGuard } from '@mateusz-d-todo-list/server/auth/infrastructure';
import { UserRole } from '@mateusz-d-todo-list/server/user/core/domain';

export function AllowedRoles(...roles: UserRole[]) {
  return applyDecorators(
    SetMetadata(metadataUserRolesKey, roles),
    UseGuards(UserRoleGuard),
  );
}

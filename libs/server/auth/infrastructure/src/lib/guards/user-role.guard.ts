import {
  CanActivate,
  ExecutionContext,
  Injectable,
  Logger,
  LoggerService,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { metadataUserRolesKey } from '@mateusz-d-todo-list/server/auth/infrastructure';
import { AuthorizedUserDataModel } from '@mateusz-d-todo-list/server/auth/core/domain';
import { UserRole } from '@mateusz-d-todo-list/server/user/core/domain';
import { isDefined } from '@mateusz-d-todo-list/shared/util-typescript';

@Injectable()
export class UserRoleGuard implements CanActivate {
  private readonly loggerService: LoggerService = new Logger(
    UserRoleGuard.name,
  );

  constructor(private readonly reflector: Reflector) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const allowedRoles =
      this.reflector.get<UserRole[]>(
        metadataUserRolesKey,
        context.getHandler(),
      ) ||
      this.reflector.get<UserRole[]>(metadataUserRolesKey, context.getClass());

    if (!isDefined(allowedRoles)) {
      this.loggerService.log(
        `List of allowed roles for requested action is not defined`,
      );
    }

    const currentUserRole = this.getCurrentUserRole(context);
    return isDefined(currentUserRole) && allowedRoles.includes(currentUserRole);
  }

  private getCurrentUserRole(context: ExecutionContext): UserRole | null {
    const request = context.switchToHttp().getRequest();
    const user = request.user as AuthorizedUserDataModel;

    if (!isDefined(user)) {
      this.loggerService.log(
        `User not defined. Perhaps AllowedRoles and Auth decorators are declared in invalid order.
        Auth user must be placed AFTER AllowedUsers (in Typescript decorators are resolved in the opposite order that they are bound top-to-bottom.`,
      );
      return null;
    } else {
      return user.role;
    }
  }
}

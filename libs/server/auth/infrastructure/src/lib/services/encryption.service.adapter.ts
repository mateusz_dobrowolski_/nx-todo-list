import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { EncryptionService } from '@mateusz-d-todo-list/server/auth/core/domain-services';

@Injectable()
export class EncryptionServiceAdapter implements EncryptionService {
  private static SALT_OR_ROUNDS = 10;

  async encode(password: string): Promise<string> {
    return await bcrypt.hash(password, EncryptionServiceAdapter.SALT_OR_ROUNDS);
  }

  async isMatch(password: string, hash: string): Promise<boolean> {
    return await bcrypt.compare(password, hash);
  }
}

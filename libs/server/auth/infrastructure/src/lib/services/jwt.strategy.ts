import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { TodoListApiConfigService } from '@mateusz-d-todo-list/server/shared/config';
import { Injectable } from '@nestjs/common';
import { AuthorizedUserDataModel } from '@mateusz-d-todo-list/server/auth/core/domain';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly config: TodoListApiConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: config.getJwtSecret(),
    });
  }

  async validate(
    decodedData: AuthorizedUserDataModel,
  ): Promise<AuthorizedUserDataModel> {
    // Based on the way JWT signing works, we're guaranteed that we're receiving a valid token that we have previously signed and issued to a valid user.
    // https://docs.nestjs.com/security/authentication#implementing-passport-jwt
    return {
      ...decodedData,
    };
  }
}

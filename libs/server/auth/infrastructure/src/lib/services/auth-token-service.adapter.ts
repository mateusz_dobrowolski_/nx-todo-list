import {
  AuthTokenService,
  RefreshTokenRepository,
} from '@mateusz-d-todo-list/server/auth/core/domain-services';
import { AccessTokenReadModel } from '@mateusz-d-todo-list/server/auth/core/application-services';
import { MILLISECONDS_IN_HOUR } from '@mateusz-d-todo-list/server/shared/domain';
import {
  AuthorizedUserDataModel,
  RefreshTokenEntityFactory,
} from '@mateusz-d-todo-list/server/auth/core/domain';
import { TodoListApiConfigService } from '@mateusz-d-todo-list/server/shared/config';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserRole } from '@mateusz-d-todo-list/server/user/core/domain';

@Injectable()
export class AuthTokenServiceAdapter implements AuthTokenService {
  constructor(
    private readonly configService: TodoListApiConfigService,
    private readonly jwtService: JwtService,
    private readonly refreshTokenRepository: RefreshTokenRepository,
  ) {}

  async createAuthToken(
    userId: number,
    email: string,
    role: UserRole,
  ): Promise<AccessTokenReadModel> {
    const expirationTimeInHours = this.configService.getJwtExpirationTimeInHours();
    const expirationRefreshTimeInHours = this.configService.getJwtRefreshExpirationTimeInHours();

    const jwtTokenData: AuthorizedUserDataModel = {
      id: userId,
      email,
      role,
    };

    const jwtToken = await this.jwtService.signAsync(jwtTokenData, {
      expiresIn: `${expirationTimeInHours}h`,
    });

    const accessTokenExpiresAt = new Date(
      Date.now() + expirationTimeInHours * MILLISECONDS_IN_HOUR,
    );

    const refreshTokenExpiresAt = new Date(
      Date.now() + expirationRefreshTimeInHours * MILLISECONDS_IN_HOUR,
    );

    const refreshTokenEntity = RefreshTokenEntityFactory.createEntity(
      userId,
      refreshTokenExpiresAt,
    );
    const savedRefreshTokenEntity = await this.refreshTokenRepository.renew(
      refreshTokenEntity,
    );

    return {
      accessToken: jwtToken,
      accessTokenExpiresAt: accessTokenExpiresAt,
      refreshToken: savedRefreshTokenEntity.uuid,
      refreshTokenExpiresAt: savedRefreshTokenEntity.expiresAt,
    };
  }
}

import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TodoListApiConfigService } from '@mateusz-d-todo-list/server/shared/config';
import { JwtStrategy } from './services/jwt.strategy';
import {
  AuthTokenService,
  EncryptionService,
  RefreshTokenRepository,
} from '@mateusz-d-todo-list/server/auth/core/domain-services';
import { EncryptionServiceAdapter } from './services/encryption.service.adapter';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RefreshTokenEntity } from '@mateusz-d-todo-list/server/auth/core/domain';
import { RefreshTokenRepositoryAdapter } from './repositories/refresh-token-repository.adapter';
import { AuthTokenServiceAdapter } from './services/auth-token-service.adapter';
import { JwtAuthGuard, UserRoleGuard } from './guards';

const typeormModule = TypeOrmModule.forFeature([RefreshTokenEntity]);

@Module({
  providers: [
    JwtAuthGuard,
    JwtStrategy,
    UserRoleGuard,
    {
      provide: EncryptionService,
      useClass: EncryptionServiceAdapter,
    },
    {
      provide: RefreshTokenRepository,
      useClass: RefreshTokenRepositoryAdapter,
    },
    {
      provide: AuthTokenService,
      useClass: AuthTokenServiceAdapter,
    },
  ],
  imports: [
    typeormModule,
    JwtModule.registerAsync({
      inject: [TodoListApiConfigService],
      useFactory: (configService: TodoListApiConfigService) => ({
        secret: configService.getJwtSecret(),
      }),
    }),
  ],
  exports: [
    JwtAuthGuard,
    JwtModule,
    EncryptionService,
    RefreshTokenRepository,
    AuthTokenService,
  ],
})
export class ServerAuthInfrastructureModule {}

export abstract class EncryptionService {
  abstract async encode(password: string): Promise<string>;

  abstract async isMatch(password: string, hash: string): Promise<boolean>;
}

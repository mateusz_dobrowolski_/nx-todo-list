import { AccessTokenReadModel } from '@mateusz-d-todo-list/server/auth/core/application-services';
import { UserRole } from '@mateusz-d-todo-list/server/user/core/domain';

export abstract class AuthTokenService {
  abstract async createAuthToken(
    userId: number,
    email: string,
    role: UserRole,
  ): Promise<AccessTokenReadModel>;
}

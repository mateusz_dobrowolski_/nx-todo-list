import {
  RefreshTokenEntity,
  RefreshTokenEntityWithUser,
} from '@mateusz-d-todo-list/server/auth/core/domain';

export abstract class RefreshTokenRepository {
  abstract delete(entity: RefreshTokenEntity): Promise<RefreshTokenEntity>;

  abstract getByUuid(
    uuid: string,
  ): Promise<RefreshTokenEntityWithUser | undefined>;

  abstract renew(entity: RefreshTokenEntity): Promise<RefreshTokenEntity>;
}

import { LoginHandler } from './login/login.handler';
import { SignUpHandler } from './sign-up/sign-up.handler';
import { RefreshTokenHandler } from './refresh-token';

export const QUERY_HANDLERS = [
  LoginHandler,
  SignUpHandler,
  RefreshTokenHandler,
];

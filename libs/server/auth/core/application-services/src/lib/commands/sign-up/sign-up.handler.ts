import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { SignUpCommand } from './sign-up.command';
import { UserRepository } from '@mateusz-d-todo-list/server/user/core/domain-services';
import { EmailAlreadyInUseException } from '@mateusz-d-todo-list/server/auth/core/domain';
import { EncryptionService } from '@mateusz-d-todo-list/server/auth/core/domain-services';
import {
  UserEntity,
  UserEntityFactory,
  UserRole,
} from '@mateusz-d-todo-list/server/user/core/domain';

@CommandHandler(SignUpCommand)
export class SignUpHandler implements ICommandHandler<SignUpCommand> {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly encryptionService: EncryptionService,
  ) {}

  async execute({ email, password }: SignUpCommand): Promise<void> {
    const user = await this.userRepository.getByEmail(email);
    if (user) {
      throw new EmailAlreadyInUseException(email);
    }
    const encodedPassword = await this.encryptionService.encode(password);
    const newUserEntity: UserEntity = UserEntityFactory.createNewUser(
      email,
      encodedPassword,
      UserRole.User,
    );
    await this.userRepository.createNewUser(newUserEntity);
  }
}

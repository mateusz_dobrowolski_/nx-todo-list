export class RefreshTokenCommand {
  constructor(public readonly refreshTokenUuid: string) {}
}

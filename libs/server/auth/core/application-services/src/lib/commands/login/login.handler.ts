import { LoginCommand } from './login.command';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AccessTokenReadModel } from '@mateusz-d-todo-list/server/auth/core/application-services';
import { UserRepository } from '@mateusz-d-todo-list/server/user/core/domain-services';
import { InvalidCredentialsException } from '@mateusz-d-todo-list/server/auth/core/domain';
import {
  AuthTokenService,
  EncryptionService,
  RefreshTokenRepository,
} from '@mateusz-d-todo-list/server/auth/core/domain-services';

@CommandHandler(LoginCommand)
export class LoginHandler implements ICommandHandler<LoginCommand> {
  constructor(
    private readonly encryptionService: EncryptionService,
    private readonly userRepository: UserRepository,
    private readonly authTokenService: AuthTokenService,
    private readonly refreshTokenRepository: RefreshTokenRepository,
  ) {}

  async execute({
    email,
    password,
  }: LoginCommand): Promise<AccessTokenReadModel> {
    const user = await this.userRepository.getByEmail(email);

    if (!user) {
      throw new InvalidCredentialsException();
    }

    if (!(await this.encryptionService.isMatch(password, user.password))) {
      throw new InvalidCredentialsException();
    }

    const newToken = await this.authTokenService.createAuthToken(
      user.id,
      user.email,
      user.role,
    );
    return newToken;
  }
}

import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AccessTokenReadModel } from '@mateusz-d-todo-list/server/auth/core/application-services';
import { JwtService } from '@nestjs/jwt';
import { UserRepository } from '@mateusz-d-todo-list/server/user/core/domain-services';
import {
  AuthTokenService,
  EncryptionService,
  RefreshTokenRepository,
} from '@mateusz-d-todo-list/server/auth/core/domain-services';
import { TodoListApiConfigService } from '@mateusz-d-todo-list/server/shared/config';
import { RefreshTokenCommand } from './refresh-token.command';
import {
  InvalidRefreshTokenException,
  RefreshTokenEntityWithUser,
} from '@mateusz-d-todo-list/server/auth/core/domain';

@CommandHandler(RefreshTokenCommand)
export class RefreshTokenHandler
  implements ICommandHandler<RefreshTokenCommand> {
  constructor(
    private readonly jwtService: JwtService,
    private readonly encryptionService: EncryptionService,
    private readonly userRepository: UserRepository,
    private readonly configService: TodoListApiConfigService,
    private readonly refreshTokenRepository: RefreshTokenRepository,
    private readonly authTokenService: AuthTokenService,
  ) {}

  async execute({
    refreshTokenUuid,
  }: RefreshTokenCommand): Promise<AccessTokenReadModel> {
    const refreshTokenEntity:
      | RefreshTokenEntityWithUser
      | undefined = await this.refreshTokenRepository.getByUuid(
      refreshTokenUuid,
    );

    if (!refreshTokenEntity) {
      throw new InvalidRefreshTokenException();
    }

    if (Date.now() > refreshTokenEntity.expiresAt.getTime()) {
      await this.refreshTokenRepository.delete(refreshTokenEntity);
      throw new InvalidRefreshTokenException();
    }

    const user = refreshTokenEntity.user;

    const newToken = await this.authTokenService.createAuthToken(
      user.id,
      user.email,
      user.role,
    );
    return newToken;
  }
}

import { ApiProperty } from '@nestjs/swagger';
import { LoginResponse } from '@mateusz-d-todo-list/shared/api-models';

export class AccessTokenReadModel implements LoginResponse {
  @ApiProperty()
  accessToken: string;

  @ApiProperty()
  accessTokenExpiresAt: Date;

  @ApiProperty()
  refreshToken: string;

  @ApiProperty()
  refreshTokenExpiresAt: Date;

  constructor({
    accessToken,
    accessTokenExpiresAt,
    refreshToken,
    refreshTokenExpiresAt,
  }: Pick<
    AccessTokenReadModel,
    | 'accessToken'
    | 'accessTokenExpiresAt'
    | 'refreshToken'
    | 'refreshTokenExpiresAt'
  >) {
    this.accessToken = accessToken;
    this.accessTokenExpiresAt = accessTokenExpiresAt;
    this.refreshToken = refreshToken;
    this.refreshTokenExpiresAt = refreshTokenExpiresAt;
  }
}

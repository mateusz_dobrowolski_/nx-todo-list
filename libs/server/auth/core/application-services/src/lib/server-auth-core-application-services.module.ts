import { DynamicModule, Module } from '@nestjs/common';
import { AuthFacade } from './services';
import { QUERY_HANDLERS } from './commands';
import { CqrsModule } from '@nestjs/cqrs';
import { ModuleInfrastructure } from '@mateusz-d-todo-list/server/shared/domain';

@Module({
  imports: [CqrsModule],
  providers: [AuthFacade, ...QUERY_HANDLERS],
  exports: [AuthFacade],
})
export class ServerAuthCoreApplicationServicesModule {
  static withInfrastructure(
    infrastructure: ModuleInfrastructure,
  ): DynamicModule {
    return {
      module: ServerAuthCoreApplicationServicesModule,
      imports: [CqrsModule, ...infrastructure],
      providers: [AuthFacade, ...QUERY_HANDLERS],
      exports: [AuthFacade],
    };
  }
}

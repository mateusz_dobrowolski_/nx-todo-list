import { Injectable } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { LoginCommand } from '../commands/login/login.command';
import { AccessTokenReadModel } from '@mateusz-d-todo-list/server/auth/core/application-services';
import { SignUpCommand } from '../commands/sign-up/sign-up.command';
import { RefreshTokenCommand } from '../commands/refresh-token';

@Injectable()
export class AuthFacade {
  constructor(private readonly commandBus: CommandBus) {}

  async login(email: string, password: string): Promise<AccessTokenReadModel> {
    return this.commandBus.execute(new LoginCommand(email, password));
  }

  async signUp(email: string, password: string): Promise<void> {
    return this.commandBus.execute(new SignUpCommand(email, password));
  }

  async refreshToken(refreshToken: string): Promise<AccessTokenReadModel> {
    return this.commandBus.execute(new RefreshTokenCommand(refreshToken));
  }
}

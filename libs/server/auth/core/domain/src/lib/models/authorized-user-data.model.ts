import { UserRole } from '@mateusz-d-todo-list/server/user/core/domain';

export interface AuthorizedUserDataModel {
  id: number;
  email: string;
  role: UserRole;
}

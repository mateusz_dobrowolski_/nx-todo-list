import { CoreBadRequestException } from '@mateusz-d-todo-list/server/shared/domain';

export class InvalidCredentialsException extends CoreBadRequestException {
  constructor() {
    super('Invalid credentials');
  }
}

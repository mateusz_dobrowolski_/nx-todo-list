import { CoreBadRequestException } from '@mateusz-d-todo-list/server/shared/domain';

export class InvalidRefreshTokenException extends CoreBadRequestException {
  constructor() {
    super('Invalid refresh token');
  }
}

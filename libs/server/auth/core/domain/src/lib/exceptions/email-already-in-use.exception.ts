import { CoreUnprocessableEntityException } from '@mateusz-d-todo-list/server/shared/domain';

export class EmailAlreadyInUseException extends CoreUnprocessableEntityException {
  constructor(email: string) {
    super(`Email ${email} is already in use.`);
  }
}

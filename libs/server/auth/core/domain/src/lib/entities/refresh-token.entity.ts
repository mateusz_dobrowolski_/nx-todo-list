import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { UserEntity } from '@mateusz-d-todo-list/server/user/core/domain';
import {
  AbstractEntity,
  EntityWithRelation,
} from '@mateusz-d-todo-list/server/shared/domain';

const userJoinColumnName = 'userId';

@Entity('refresh_token')
export class RefreshTokenEntity extends AbstractEntity {
  @PrimaryGeneratedColumn('uuid')
  uuid: string;

  @ManyToOne(() => UserEntity)
  @JoinColumn({ name: userJoinColumnName })
  user?: UserEntity;

  @Column({ type: 'int' })
  [userJoinColumnName]: number;

  @Column({ type: 'timestamp with time zone' })
  expiresAt: Date;
}

export type RefreshTokenEntityWithUser = EntityWithRelation<
  RefreshTokenEntity,
  'user'
>;

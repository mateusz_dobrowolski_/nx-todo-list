import { RefreshTokenEntity } from './refresh-token.entity';

export class RefreshTokenEntityFactory {
  static createEntity(userId: number, expiresAt: Date): RefreshTokenEntity {
    const newEntity = new RefreshTokenEntity();
    newEntity.userId = userId;
    newEntity.expiresAt = expiresAt;
    return newEntity;
  }
}

import { MigrationInterface, QueryRunner } from 'typeorm';

export class addUserRole1631378688063 implements MigrationInterface {
  name = 'addUserRole1631378688063';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE "UserRole_enum" AS ENUM('user', 'admin')`,
    );
    await queryRunner.query(
      `ALTER TABLE "user"
        ADD "role" "UserRole_enum"`,
    );
    await queryRunner.query(`UPDATE "user"
                             SET "role" = 'user'`);
    await queryRunner.query(
      `ALTER TABLE "user"
        ALTER COLUMN "role" SET NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "role"`);
    await queryRunner.query(`DROP TYPE "UserRole_enum"`);
  }
}

## Frontend

### Development

```
npm run start
```

## API

### Docker

Currently only database is dockerized. Launch docker before running an app or CLI migrations.

```
 docker-compose up
```

```
 docker-compose down
```

### Logging

If you want to log all unexpected errors you can simply pipe the stdout to a file, like:
``node main.js > app.log``

### Development

```
npm run start api
```

### Swagger

```
http://localhost:3333/doc/
```

### Migrations

#### Generate migrations

(based on difference between entities and db schema)

```
npm run typeorm:generate migrationName
```

#### Create empty migration

```
npm run typeorm:create migrationName
```

#### Run migrations up

```
npm run typeorm:up
```

#### Run migrations down

```
npm run typeorm:down
```

import { Module } from '@nestjs/common';
import { ServerTaskApiRestModule } from '@mateusz-d-todo-list/server/task/api-rest';
import { ServerSharedCoreModule } from '@mateusz-d-todo-list/server/shared/core';
import { ServerSharedCoreApiRestModule } from '@mateusz-d-todo-list/server/shared/core-api-rest';
import { ServerAuthApiRestModule } from '@mateusz-d-todo-list/server/auth/api-rest';
import { ServerUserApiRestModule } from '@mateusz-d-todo-list/server/user/api-rest';

@Module({
  imports: [
    ServerSharedCoreApiRestModule,
    ServerSharedCoreModule,
    ServerTaskApiRestModule,
    ServerAuthApiRestModule,
    ServerUserApiRestModule,
  ],
})
export class AppModule {}

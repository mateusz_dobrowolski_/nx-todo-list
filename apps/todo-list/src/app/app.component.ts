import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'mateusz-d-todo-list-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {}

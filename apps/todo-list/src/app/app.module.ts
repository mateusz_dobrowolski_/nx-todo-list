import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { WebSharedCoreModule } from '@mateusz-d-todo-list/web/shared/core';
import { WebTaskShellModule } from '@mateusz-d-todo-list/web/task/shell';
import { WebAuthShellModule } from '@mateusz-d-todo-list/web/auth/shell';

@NgModule({
  declarations: [AppComponent],
  imports: [
    AppRoutingModule,
    WebSharedCoreModule,
    WebTaskShellModule,
    WebAuthShellModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
